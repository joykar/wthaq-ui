import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovEntityDashboardComponent } from './gov-entity-dashboard.component';

describe('GovEntityDashboardComponent', () => {
  let component: GovEntityDashboardComponent;
  let fixture: ComponentFixture<GovEntityDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovEntityDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovEntityDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
