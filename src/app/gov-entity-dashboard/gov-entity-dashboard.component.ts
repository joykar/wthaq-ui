import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { LGIssuanceModel } from '../models/lgIssuance.model';

@Component({
  selector: 'app-gov-entity-dashboard',
  templateUrl: './gov-entity-dashboard.component.html',
  styleUrls: ['./gov-entity-dashboard.component.css']
})
export class GovEntityDashboardComponent implements OnInit {

  userId: string;
  isOpenPendingList: boolean = true;
  isOpenLGList: boolean = false;
  pendingList: any[] = [];
  isPendingClicked: boolean = true;
  isLGClicked: boolean = false;
  bankNamesList: any[] = [];
  bankName: string;
  userName: string;

  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService) { }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    console.log("this.userId::::" + this.userId);
    this.userName = localStorage.getItem('userName');
    console.log("this.userName::::" + this.userName);

    //To get pending list
    this.commonService.getAllLGRequestBeneficiarySpecific(this.userId)
      .subscribe(res => {
        console.log("AllLGRequestBeneficiarySpecific list::::" + JSON.stringify(res));
        this.pendingList = res.userObject;
        console.log("pendingList  is::::" + JSON.stringify(this.pendingList));

      });


  }
  goToPendingReqTab() {
    this.isOpenPendingList = true;
    this.isOpenLGList = false;
    this.isPendingClicked = true;
    this.isLGClicked = false;

    //To get pending list
    this.commonService.getAllLGRequestBeneficiarySpecific(this.userId)
      .subscribe(res => {
        console.log("AllLGRequestBeneficiarySpecific list::::" + JSON.stringify(res));
        this.pendingList = res.userObject;
        console.log("pending list is::::" + JSON.stringify(this.pendingList));

      });
  }

  goToLGListTab() {
    this.isOpenPendingList = false;
    this.isOpenLGList = true;
    this.isPendingClicked = false;
    this.isLGClicked = true;


  }

  RowSelected(pendingData){
    console.log(JSON.stringify(pendingData));
   this.commonService.lgIssuanceApproval = new LGIssuanceModel();
    if(pendingData.lg_type !== null || pendingData.lg_type !== undefined){
     this.commonService.lgIssuanceApproval.LGType = pendingData.lg_type;
    }

    if(pendingData.bankName !== null || pendingData.bankName !== undefined){
     this.commonService.lgIssuanceApproval.bankName = pendingData.bankName;
    }
    if(pendingData.lg_startDate !== null || pendingData.lg_startDate !== undefined){
     this.commonService.lgIssuanceApproval.LGStartDate = pendingData.lg_startDate;
    }
    if(pendingData.lg_endDate !== null || pendingData.lg_endDate !== undefined){
     this.commonService.lgIssuanceApproval.LGEndDate = pendingData.lg_endDate;
    }
    if(pendingData.lgAmount !== null || pendingData.lgAmount !== undefined){
     this.commonService.lgIssuanceApproval.Amount = pendingData.lgAmount;
    }
    if(pendingData.currency !== null || pendingData.currency !== undefined){
     this.commonService.lgIssuanceApproval.currency = pendingData.currency;
    }
    if(pendingData.requestId !== null || pendingData.requestId !== undefined){
      this.commonService.lgIssuanceApproval.requestId = pendingData.requestId;
     }
     if(pendingData.terms_condition_message !== null || pendingData.terms_condition_message !== undefined){
      this.commonService.lgIssuanceApproval.termsNconditionsMessage = pendingData.terms_condition_message;
     }
     if(pendingData.response_code !== null || pendingData.response_code !== undefined){
      this.commonService.lgIssuanceApproval.response_code = pendingData.response_code;
      console.log("............................pendingData.response_code"+pendingData.response_code);

     }


     if(pendingData.request_id !== null || pendingData.request_id !== undefined){
      this.commonService.lgIssuanceApproval.request_id = pendingData.request_id;
      console.log("........................pendingData.request_id"+pendingData.request_id);
     }

     if(pendingData.create_by !== null || pendingData.create_by !== undefined){
      this.commonService.lgIssuanceApproval.create_by = pendingData.create_by;

     }

     console.log("####@@@@:::::"+JSON.stringify(this.commonService.lgIssuanceApproval));


    this.router.navigate(['requestLgIssuanceGovApproval']);


    //this.lgIssuance.

  }

  logout()
  {
    this.commonService.logout(this.userId);
    
  }
}
