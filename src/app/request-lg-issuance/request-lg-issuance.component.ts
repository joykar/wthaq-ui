import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { LGIssuanceModel } from '../models/lgIssuance.model';
import { Message } from 'primeng/api';
import { MessageService } from 'primeng/api';

declare var require: any

@Component({
  selector: 'app-request-lg-issuance',
  templateUrl: './request-lg-issuance.component.html',
  styleUrls: ['./request-lg-issuance.component.css']
})
export class RequestLgIssuanceComponent implements OnInit {


  msgs: Message[] = [];
  lgTypeList: any[] = [];
  currencyList: any[] = [];
  bankNamesList: any[] = [];
  beneficiaryList: any[] = [];
  writeUpList: any[] = [];
  writeUpType: string;
  userId: string;
  checkBox: boolean = false;
  privateEntityCheckBox: boolean = false;
  isChecked: string = 'n';
  selectedWriteupType: string;
  startDate: Date;
  endDate: Date;
  zakatStartDate: Date;
  zakatEndDate: Date;
  bId: any;
  bankCode: string;
  currencyCode: string;
  amountInWords: string;
  isOpenValidationAlert: boolean = false;
  //termsNconditionsMessage: string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
  // TermsAndCondContent: string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
  // defaultTermsAndCondContent: string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
  TermsAndCondContent: string;
  defaultTermsAndCondContent: string;
  // SAMATermsAndCondContent: string;
  // NONSAMATermsAndCondContent: string;
  CRNumberList: any[] = [];
  crNumber: string = "";
  isEditTnC: boolean = false;
  //isEnableSelectEntity: boolean = true;
  isEnableCRNumber: boolean = true;
  lgIssuance: LGIssuanceModel;
  submitLG: FormGroup;
  noPattern: string = '^(0|[1-9][0-9]*)$';
  textPattern = "^[A-Za-z ]*$";
  alphanumericPattern = "^[a-zA-Z0-9 ]+$";
  amountPattern = "^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$";
  //amountPattern = "^-\d+$";
  //amountPattern = "^\-?[1-9]\d{0,2}(\.\d*)?$";
  percentageOfContractValuePattern = "^[0-9][0-9]?$|^100$";


  selectedLGTypeName = "Bid Bond";
  selecteWriteUpTypeName = "SAMA";
  isEnablebayanNuymber: boolean = false;
  isEnableZakatPeriodStartDate: boolean = false;
  isEnableZakatPeriodEndDate: boolean = false;
  minDate: Date;
  minDateforEndDate: Date;
  minDateforzakatEndDate: Date;
  DefaultSAMATermsAndCondContent: string;
  //DefaultNONSAMATermsAndCondContent: string;
  roleId: any;
  loginId: any;
  ninNumber: any;



  validation_messages = {
    LGType: [
      { type: "required", message: "This is a required field." }
    ],
    writeUpType: [
      { type: "required", message: "This is a required field." }
    ],
    bankName: [
      { type: "required", message: "This is a required field." }
    ],
    IBAN: [
      { type: "required", message: "This is a required field." }
    ],
    beneficiaryId: [
      { type: "required", message: "This is a required field." }
    ],
    CR: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "CRN number should be numeric."
      },
      {
        type: "maxlength",
        message: "CRN number should be 15 digits number."
      },
      {
        type: "minlength",
        message: "CRN number should be 15 digits number."
      }
    ],
    Amount: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Amount should be composed of digits. It accepts only positive numbers."
      }

    ],
    amountInNumber: [
      { type: "required", message: "This is a required field." }
    ],
    currency: [
      { type: "required", message: "Required field." }
    ],
    LGStartDate: [
      { type: "required", message: "This is a required field." }
    ],
    LGEndDate: [
      { type: "required", message: "This is a required field." }
    ],
    projectName: [
      {
        type: "pattern",
        message: "Project Name should be composed of alphanumeric characters."
      }
    ],
    projectNumber: [
      {
        type: "pattern",
        message: "Project Number should be composed of alphanumeric characters."
      }
    ],
    ZakatPeriodStartDate: [
      { type: "required", message: "This is a required field." }
    ],
    ZakatPeriodEndDate: [
      { type: "required", message: "This is a required field." }
    ],
    puroseOfBond: [
      { type: "required", message: "This is a required field." },
      {
        type: "maxlength",
        message: "You can enter purose of bond of maximum 300 characters."
      }
    ],
    bayanNuymber: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Bayan Number should be alphanumeric. Special characters not allowed."
      },
    ],
    percentageOfContractValue: [
      {
        type: "pattern",
        message: " Percentage of contract matches from 0 to 100."
      },
    ]

  }

  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService,
    private messageService: MessageService) {

  }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    console.log("this.userId::::" + this.userId);
    this.roleId = localStorage.getItem('roleId');
    console.log("this.roleId::::" + this.roleId);
    console.log("this.selecteWriteUpTypeName::::" + this.writeUpType);
    this.loginId = localStorage.getItem('loginId');
    console.log("this.loginId::::" + this.loginId);



    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    this.minDate = new Date();
    this.minDate.setMonth(month);
    this.minDate.setFullYear(year);
    //console.log("this.startDate::::"+this.startDate);

    //To get Lg Type
    this.commonService.getLGTypeList()
      .subscribe(res => {
        console.log("LG list::::" + JSON.stringify(res));
        this.lgTypeList = res;
        console.log("lgTypeList list is::::" + JSON.stringify(this.lgTypeList));


      });

    //to get write-up type
    this.commonService.getWriteUpList()
      .subscribe(res => {
        console.log("WriteUp list::::" + JSON.stringify(res));
        this.writeUpList = res;
        console.log("writeUp list is::::" + JSON.stringify(this.writeUpList));
        for (let w of this.writeUpList) {
          if (w.writeuptype === 'SAMA') {
            this.TermsAndCondContent = w.termsConditionMessage;
            this.defaultTermsAndCondContent = w.termsConditionMessage
            break;

          }
        }

      });



    //To get currency list
    this.commonService.getCurrencyList()
      .subscribe(res => {
        console.log("Currency list::::" + JSON.stringify(res));
        this.currencyList = res;
        console.log("Currency list is::::" + JSON.stringify(this.currencyList));

      });

    //To get IBAN
    this.commonService.getIBANList(this.userId)
      .subscribe(res => {
        console.log("IBAN list::::" + JSON.stringify(res));
        this.bankNamesList = res;
        console.log(this.bankNamesList);


      });

    //To get beneficiary
    this.commonService.getSelectGovermentEntityRegistered()
      .subscribe(res => {
        console.log("benificiary list::::" + JSON.stringify(res));
        this.beneficiaryList = res;
        console.log(this.beneficiaryList);


      });

    //get CRN
    this.commonService.getSelectedCRN(this.userId)
      .subscribe(res => {
        this.CRNumberList = res.userObject;
        for (let cr of this.CRNumberList) {
          console.log("CRN::::" + JSON.stringify(cr.crnNo));
          this.crNumber = cr.crnNo;
          break;
        }
        console.log(this.crNumber);
        //this.submitLG.controls["CR"].setValue(this.crNumber);




      });

    this.buildForm();
    this.setLGCategoryValidators();

    // this.submitLG = this.formBuilder.group({
    //   LGType: [
    //     this.lgTypeList[1],
    //     [
    //       Validators.required,

    //     ]
    //   ],
    //   writeUpType: [
    //     "",
    //     [
    //       Validators.required,

    //     ]
    //   ],
    //   bankName: [
    //     "",
    //     [
    //       Validators.required,

    //     ]
    //   ],
    //   IBAN: [
    //     "",
    //     [
    //       Validators.required,

    //     ]
    //   ],
    //   beneficiaryId: [
    //     "",
    //     [
    //       Validators.required,

    //     ]
    //   ],
    //   CR: [
    //     "",
    //     [
    //       Validators.required,
    //       Validators.pattern(this.noPattern),
    //       Validators.maxLength(15),
    //       Validators.minLength(15)

    //     ]
    //   ],
    //   disableCR: [
    //     ""
    //   ],
    //   Amount: [
    //     "",
    //     [
    //       Validators.required,
    //       Validators.pattern(this.amountPattern),


    //     ]
    //   ],
    //   amountInNumber: [
    //     "",
    //     [
    //       Validators.required,

    //     ]
    //   ],
    //   currency: [
    //     "",
    //     [
    //       Validators.required,

    //     ]
    //   ],
    //   LGStartDate: [
    //     null,
    //     [
    //       Validators.required
    //     ]
    //   ],
    //   LGEndDate: [
    //     null,
    //     [
    //       Validators.required

    //     ]
    //   ],
    //   projectName: [
    //     "",
    //     [
    //       Validators.pattern(this.alphanumericPattern)

    //     ]
    //   ],
    //   projectNumber: [
    //     "",
    //     [
    //       Validators.pattern(this.alphanumericPattern)

    //     ]
    //   ],
    //   ZakatPeriodStartDate: [
    //     ""
    //   ],
    //   ZakatPeriodEndDate: [
    //     ""
    //   ],
    //   puroseOfBond: [
    //     "",
    //     [
    //       Validators.required,
    //       Validators.maxLength(300)

    //     ]
    //   ],
    //   bayanNuymber: [
    //     "",
    //     [
    //       Validators.required,
    //       Validators.pattern(this.alphanumericPattern)

    //     ]
    //   ],
    //   checkBox: [
    //     ""
    //   ],
    //   TermsAndCondContent: [
    //     this.TermsAndCondContent
    //   ],
    //   privateEntityCheckBox: [
    //     ""
    //   ],
    //   disabledBayanNuymber: [
    //     ""
    //   ],
    //   disabledZakatPeriodStartDate: [
    //     ""
    //   ],
    //   disabledZakatPeriodEndtDate: [
    //     ""
    //   ],
    //   percentageOfContractValue: [
    //     "",
    //     [
    //       Validators.pattern(this.percentageOfContractValuePattern),
    //     ]
    //   ]

    // })

    // if(){
    this.submitLG.get('IBAN').disable();
    // }

    if (this.privateEntityCheckBox) {
      this.submitLG.get('beneficiaryId').disable();
      this.isEnableCRNumber = true;
      this.isEditTnC = true;
      console.log("in if");
      console.log("this.isEnableCRNumber" + this.isEnableCRNumber);
      console.log("this.isEditTnC" + this.isEditTnC);

    }
    else {

      this.submitLG.get('beneficiaryId').enable();
      this.isEnableCRNumber = false;
      this.isEditTnC = false;
      console.log("in else");
      console.log("this.isEnableCRNumber" + this.isEnableCRNumber);
      console.log("this.isEditTnC" + this.isEditTnC);
    }

    if (this.writeUpType === undefined) {
      return this.privateEntityCheckBox.valueOf() == true;
    }

  }/* end of ngOnInit() */

  buildForm() {

    this.submitLG = this.formBuilder.group({

      LGType: [this.lgTypeList[1]],
      writeUpType: [""],
      bankName: ["", [Validators.required]],
      IBAN: ["", [Validators.required]],
      beneficiaryId: [""],
      CR: ["", [Validators.pattern(this.noPattern), Validators.maxLength(15), Validators.minLength(15)]],
      disableCR: [""],
      Amount: ["", [Validators.required, Validators.pattern(this.amountPattern)]],
      amountInNumber: ["", [Validators.required]],
      currency: ["", [Validators.required]],
      LGStartDate: [null, [Validators.required]],
      LGEndDate: [null, [Validators.required]],
      projectName: ["", [Validators.pattern(this.alphanumericPattern)]],
      projectNumber: ["", [Validators.pattern(this.alphanumericPattern)]],
      ZakatPeriodStartDate: [null],
      ZakatPeriodEndDate: [null],
      puroseOfBond: ["", [Validators.required, Validators.maxLength(300)]],
      bayanNuymber: ["", [Validators.pattern(this.alphanumericPattern)]],
      checkBox: [""],
      TermsAndCondContent: [this.TermsAndCondContent],
      privateEntityCheckBox: [""],
      disabledBayanNuymber: [""],
      disabledZakatPeriodStartDate: [""],
      disabledZakatPeriodEndtDate: [""],
      percentageOfContractValue: ["", [Validators.pattern(this.percentageOfContractValuePattern)]],

    });
  }

  setLGCategoryValidators() {

    const beneficiaryIdControl = this.submitLG.get('beneficiaryId');
    const CRControl = this.submitLG.get('CR');
    const ZakatPeriodStartDateControl = this.submitLG.get('ZakatPeriodStartDate');
    const ZakatPeriodEndDateControl = this.submitLG.get('ZakatPeriodEndDate');
    const bayanNuymberControl = this.submitLG.get('bayanNuymber');

    this.submitLG.get('privateEntityCheckBox').valueChanges
      .subscribe(privateEntityCheckBoxValue => {

        if (privateEntityCheckBoxValue) {
          beneficiaryIdControl.setValidators(null);
          CRControl.setValidators([Validators.required]);
        } else {
          beneficiaryIdControl.setValidators([Validators.required]);
          CRControl.setValidators(null);
        }

        // if (writeUpType === 'SAMA' && this.privateEntityCheckBox === true) {
        //   CRControl.setValidators([Validators.required]);
        //   beneficiaryIdControl.setValidators(null);
        // }
        // if (writeUpType === undefined && this.privateEntityCheckBox === true) {
        //   CRControl.setValidators([Validators.required]);
        //   beneficiaryIdControl.setValidators(null);
        // }
        // if (writeUpType === 'SAMA' && this.privateEntityCheckBox === false) {
        //   CRControl.setValidators(null);
        //   beneficiaryIdControl.setValidators([Validators.required]);
        // }
        // if (writeUpType === undefined && this.privateEntityCheckBox === false) {
        //   CRControl.setValidators(null);
        //   beneficiaryIdControl.setValidators([Validators.required]);
        // }

        // if (writeUpType === 'NONSAMA') {
        //   CRControl.setValidators([Validators.required]);
        //   beneficiaryIdControl.setValidators(null);
        // }


        CRControl.updateValueAndValidity();
        beneficiaryIdControl.updateValueAndValidity();

      });

    this.submitLG.get('LGType').valueChanges
      .subscribe(LGType => {

        if (LGType === 'Zakat Guarantee Bond') {
          ZakatPeriodStartDateControl.setValidators([Validators.required]);
          ZakatPeriodEndDateControl.setValidators([Validators.required]);
        }
        else {
          ZakatPeriodStartDateControl.setValidators(null);
          ZakatPeriodEndDateControl.setValidators(null);
        }
        if (LGType === 'Custom Guarantee Bond') {
          bayanNuymberControl.setValidators([Validators.required]);

        }
        else {
          bayanNuymberControl.setValidators(null);
        }

        ZakatPeriodStartDateControl.updateValueAndValidity();
        ZakatPeriodEndDateControl.updateValueAndValidity();
        bayanNuymberControl.updateValueAndValidity();
      })

  }

  getStartDate(startDate) {
    console.log("startDate::::" + startDate);
    this.submitLG.controls['LGEndDate'].setValue(null);
    var d = new Date(startDate);
    var date = d.setDate(d.getDate() + 1);
    var month = d.getMonth(); // Since getMonth() returns month from 0-11 not 1-12
    var year = d.getFullYear();
    this.minDateforEndDate = new Date(d.setDate(d.getDate()));
    this.minDateforEndDate.setMonth(month);
    this.minDateforEndDate.setFullYear(year);

  }

  getZakatStartDate(zakatStartDate) {
    console.log("zakatStartDate::::" + zakatStartDate);
    this.submitLG.controls['ZakatPeriodEndDate'].setValue(null);
    var d = new Date(zakatStartDate);
    var date = d.setDate(d.getDate() + 1);
    var month = d.getMonth(); // Since getMonth() returns month from 0-11 not 1-12
    var year = d.getFullYear();
    this.minDateforzakatEndDate = new Date(d.setDate(d.getDate()));
    this.minDateforzakatEndDate.setMonth(month);
    this.minDateforzakatEndDate.setFullYear(year);
  }

  onClickLGType(event) {
    console.log("Selected lg type::::" + event);
    if (event === 'Custom Guarantee Bond') {
      this.isEnablebayanNuymber = true;
    }
    else {
      this.isEnablebayanNuymber = false;
    }
    if (event === 'Zakat Guarantee Bond') {
      this.isEnableZakatPeriodStartDate = true;
      this.isEnableZakatPeriodEndDate = true;
    }
    else {

      this.isEnableZakatPeriodStartDate = false;
      this.isEnableZakatPeriodEndDate = false;
    }
  }

  onClick(event) {
    console.log("selected write-up type::::" + event);
    console.log("*******" + this.privateEntityCheckBox);
    /* **** */
    this.submitLG.controls['beneficiaryId'].setValue("Select entity");
    this.submitLG.controls['CR'].setValue(null);
    this.submitLG.controls['TermsAndCondContent'].setValue(this.TermsAndCondContent);
    this.submitLG.get('privateEntityCheckBox').enable();
    this.privateEntityCheckBox = false;
    for (let w of this.writeUpList) {
      if (event === w.writeuptype) {
        this.TermsAndCondContent = w.termsConditionMessage;
        break;
      }
    }
    /* ****** */
    if (event === 'SAMA') {

      if (this.privateEntityCheckBox) {
        this.submitLG.get('beneficiaryId').disable();
        this.isEnableCRNumber = true;
        this.isEditTnC = true;
        console.log("in if");
        console.log("this.isEnableCRNumber" + this.isEnableCRNumber);
        console.log("this.isEditTnC" + this.isEditTnC);

      }
      else {

        this.submitLG.get('beneficiaryId').enable();
        this.isEnableCRNumber = false;
        this.isEditTnC = false;
        console.log("in else");
        console.log("this.isEnableCRNumber" + this.isEnableCRNumber);
        console.log("this.isEditTnC" + this.isEditTnC);
      }

    }
    else if (event === 'NONSAMA') {
      // this.selectedWriteupType = 'NONSAMA'
      this.submitLG.controls['privateEntityCheckBox'].setValue(true);
      this.submitLG.get('privateEntityCheckBox').disable();
      this.submitLG.get('beneficiaryId').disable();
      this.isEnableCRNumber = true;
      this.isEditTnC = true;
      console.log("privateEntityCheckBox:::::" + this.privateEntityCheckBox);

    }

  }

  onBlur(crn) {
    console.log("crn:::::" + crn);
    this.checkSupplierCR(crn);
  }

  checkSupplierCR(crn) {
    this.commonService.checkSupplierCR(crn)
      .subscribe(res => {
        console.log("Response for CRN is::::" + JSON.stringify(res));
        if (res.responseCode === 0) {
          alert(res.responseMessage);
          this.submitLG.controls["CR"].setValue(null);
        }
        else if (res.responseCode === 1) {
          //alert(res.responseMessage);
          if (this.bId === undefined || this.bId === null) {
            for (let user of res.userObject) {
              //this.bId = user.supplierId;
              this.ninNumber = user.loginId;
            }

          }

        }
        else if (res.responseCode === 2) {
          alert(res.responseMessage);
          this.submitLG.controls["CR"].setValue(null);
        }

      })
  }

  isCRNumberEnabled() {

    return this.privateEntityCheckBox.valueOf() == true;

  }

  onClickBenificiary(event) {
    console.log("Selected benificiary::::" + event);
    for (let b of this.beneficiaryList) {
      if (b[1] === event) {
        this.bId = b[0];
        break;
      }
    }
  }

  onClickCurrency(event) {
    console.log("Selected Currency::::" + event);
    for (let c of this.currencyList) {
      if (c[0] === event) {
        this.currencyCode = c[0];
        console.log("Selected Currency::::" + this.currencyCode);
        break;
      }
    }
  }

  onClickBank(event) {
    console.log("Selected bank::::" + event);
    if (event === 'Select Bank') {
      this.submitLG.controls['IBAN'].setValue("Select IBAN");
      this.submitLG.get('IBAN').disable();
    }
    else {
      this.submitLG.get('IBAN').enable();
    }

    for (let b of this.bankNamesList) {
      if (b.tBankmaster.bankName === event) {
        this.bankCode = b.tBankmaster.bankCode;
        break;
      }
    }
  }

  onKeyup(num) {
    console.log("Selected amount::::" + num);
    var converter = require('number-to-words');
    this.amountInWords = converter.toWords(num);
    console.log(this.amountInWords);
    this.submitLG.controls['amountInNumber'].setValue(this.amountInWords);

  }

  toggleVisibility(e) {
    if (e.target.checked) {
      this.checkBox = true;
      this.isChecked = 'y';

    }
    else {
      this.checkBox = false;
      this.isChecked = 'n';

    }
    this.submitLG.controls['checkBox'].setValue(this.checkBox);
  }


  toggleVisibilityPrivateEntity(e) {

    var writeUpType = this.submitLG.get('writeUpType').value;
    console.log("writeUpType" + writeUpType);
    this.submitLG.controls['CR'].setValue(null);
    if (writeUpType === 'SAMA' || writeUpType === undefined) {
      this.submitLG.controls['TermsAndCondContent'].setValue(this.defaultTermsAndCondContent);
    }
    else{
      this.submitLG.controls['TermsAndCondContent'].setValue(this.defaultTermsAndCondContent);
    }


    console.log("private entity::::" + e.target.checked);
    if (e.target.checked) {
      this.privateEntityCheckBox = true;
      if (writeUpType === 'SAMA') {
        this.submitLG.controls['beneficiaryId'].setValue("Select entity");
        this.submitLG.get('beneficiaryId').disable();
        this.isEnableCRNumber = true;
        this.isEditTnC = true;
        console.log("in if when checkbox clicked");
        console.log("this.isEnableCRNumber" + this.isEnableCRNumber);
        console.log("this.isEditTnC" + this.isEditTnC);
      }
      if (writeUpType === undefined) {
        this.submitLG.controls['beneficiaryId'].setValue("Select entity");
        this.submitLG.get('beneficiaryId').disable();
        this.isEnableCRNumber = true;
        this.isEditTnC = true;
        console.log("in if when checkbox clicked");
        console.log("this.isEnableCRNumber" + this.isEnableCRNumber);
        console.log("this.isEditTnC" + this.isEditTnC);
      }

    }
    else {
      this.privateEntityCheckBox = false;
      if (writeUpType === 'SAMA') {
        this.submitLG.get('beneficiaryId').enable();
        this.isEnableCRNumber = false;
        this.isEditTnC = false;
        console.log("in if when checkbox clicked");
        console.log("this.isEnableCRNumber" + this.isEnableCRNumber);
        console.log("this.isEditTnC" + this.isEditTnC);
      }
      if (writeUpType === undefined) {
        this.submitLG.get('beneficiaryId').enable();
        this.isEnableCRNumber = false;
        this.isEditTnC = false;
        console.log("in if when checkbox clicked");
        console.log("this.isEnableCRNumber" + this.isEnableCRNumber);
        console.log("this.isEditTnC" + this.isEditTnC);
      }

    }
    this.submitLG.controls['privateEntityCheckBox'].setValue(this.privateEntityCheckBox);

  }

  updateTnC() {
    this.isEditTnC = false;
  }


  submitLGIssuance() {

    this.lgIssuance = new LGIssuanceModel();

    console.log("this.submitLG.value****" + JSON.stringify(this.submitLG.value));
    if (this.submitLG.valid) {

      if (this.submitLG.value.LGType === null || this.submitLG.value.LGType === undefined || this.submitLG.value.LGType === "") {
        this.lgIssuance.LGType = this.selectedLGTypeName;
      }
      else {
        this.lgIssuance.LGType = this.submitLG.value.LGType;
      }
      if (this.submitLG.value.writeUpType === null || this.submitLG.value.writeUpType === undefined || this.submitLG.value.writeUpType === "") {
        this.lgIssuance.writeUpType = this.selecteWriteUpTypeName;
      }
      else {
        this.lgIssuance.writeUpType = this.submitLG.value.writeUpType;
      }


      this.lgIssuance.bankCode = this.bankCode;
      this.lgIssuance.IBAN = this.submitLG.value.IBAN;
      // this.lgIssuance.beneficiaryId = this.bId;
      this.lgIssuance.beneficiaryId = this.submitLG.value.beneficiaryId;
      this.lgIssuance.ninNumber = this.ninNumber;
      this.lgIssuance.CR = this.submitLG.value.CR;
      this.lgIssuance.Amount = this.submitLG.value.Amount;
      this.lgIssuance.amountInNumber = this.submitLG.value.amountInNumber;
      this.lgIssuance.currency = this.currencyCode;
      this.lgIssuance.LGStartDate = this.startDate.toLocaleDateString();
      this.lgIssuance.LGEndDate = this.endDate.toLocaleDateString();
      if (this.submitLG.value.ZakatPeriodStartDate === null || this.submitLG.value.ZakatPeriodStartDate === undefined || this.submitLG.value.ZakatPeriodStartDate === "") {
        this.lgIssuance.ZakatPeriodStartDate = "";
      }
      else {
        this.lgIssuance.ZakatPeriodStartDate = this.zakatStartDate.toLocaleDateString();
      }
      if (this.submitLG.value.ZakatPeriodEndDate === null || this.submitLG.value.ZakatPeriodEndDate === undefined || this.submitLG.value.ZakatPeriodEndDate === "") {
        this.lgIssuance.ZakatPeriodEndDate = "";
      }
      else {
        this.lgIssuance.ZakatPeriodEndDate = this.zakatEndDate.toLocaleDateString();
      }

      this.lgIssuance.projectName = this.submitLG.value.projectName;
      this.lgIssuance.projectNumber = this.submitLG.value.projectNumber;
      this.lgIssuance.puroseOfBond = this.submitLG.value.puroseOfBond;
      if (this.submitLG.value.bayanNuymber === null || this.submitLG.value.bayanNuymber === undefined || this.submitLG.value.bayanNuymber === "") {
        this.lgIssuance.bayanNuymber = "";
      }
      else {
        this.lgIssuance.bayanNuymber = this.submitLG.value.bayanNuymber;
      }

      this.lgIssuance.checkBox = this.isChecked;
      this.lgIssuance.termsNconditionsMessage = this.submitLG.value.TermsAndCondContent;
      this.lgIssuance.privateEntityCheckBox = this.privateEntityCheckBox;
      //this.lgIssuance.privateEntityCheckBox = this.submitLG.value.privateEntityCheckBox;

      console.log("this.lgIssuance::::" + JSON.stringify(this.lgIssuance));
      this.commonService.postLGIssuanceDetails(this.lgIssuance, this.userId)
        .subscribe(res => {
          console.log("Reg final page Response is::::" + JSON.stringify(res));
          if(res.userObject !== undefined || res.userObject !== null){


            this.commonService.lgIssuanceApproval = new LGIssuanceModel();
             if(res.userObject.lgTypeId !== null || res.userObject.lgTypeId !== undefined){
               for(let lg of this.lgTypeList){
                 if(lg[0] === res.userObject.lgTypeId){
                   this.commonService.lgIssuanceApproval.LGType = lg[1];
                 }
               }
             }
             // if(pendingData.beneficiary_name !== null || pendingData.beneficiary_name !== undefined){
             //   console.log("test beneficiary_name"+pendingData.beneficiary_name);
             //   this.commonService.lgIssuanceApproval.beneficiary_name = pendingData.beneficiary_name;
             //  }
             if(res.userObject.bankCode !== null || res.userObject.bankCode !== undefined){
               for(let bank of this.bankNamesList){
                 if(bank.tBankmaster.bankCode === res.userObject.bankCode){
                   this.commonService.lgIssuanceApproval.bankName = bank.tBankmaster.bankName;
                 }
               }
             }

             // if(pendingData.project_name !== null || pendingData.project_name !== undefined){
             //   console.log("bank_name"+pendingData.project_name);
             //  this.commonService.lgIssuanceApproval.project_name = pendingData.project_name;
             // }


             // if(pendingData.request_date !== null || pendingData.request_date !== undefined){
             //   console.log("bank_name"+pendingData.request_date);
             //  this.commonService.lgIssuanceApproval.request_date = pendingData.request_date;
             // }




             // if(pendingData.request_status_flag !== null || pendingData.request_status_flag !== undefined){
             //   console.log("bank_name"+pendingData.request_status_flag);
             //  this.commonService.lgIssuanceApproval.status = pendingData.request_status_flag;
             // }





             if(res.userObject.strtDt !== null || res.userObject.strtDt !== undefined){
               this.commonService.lgIssuanceApproval.LGStartDate = res.userObject.strtDt;
              }
              if(res.userObject.endDt !== null || res.userObject.endDt !== undefined){
               this.commonService.lgIssuanceApproval.LGEndDate = res.userObject.endDt;
              }

             if(res.userObject.contractValue !== null || res.userObject.contractValue !== undefined){
              this.commonService.lgIssuanceApproval.Amount = res.userObject.contractValue;
             }

             if(res.userObject.currencyCode !== null || res.userObject.currencyCode !== undefined){
               for(let currency of this.currencyList){
                 if(currency[0] === res.userObject.currencyCode){
                   this.commonService.lgIssuanceApproval.currency = currency[1];
                 }
               }
              }
              if(res.userObject.termsConditionMessage !== null || res.userObject.termsConditionMessage !== undefined){
               this.commonService.lgIssuanceApproval.termsNconditionsMessage = res.userObject.termsConditionMessage;
              }

             //  if(pendingData.status !== null || pendingData.status !== undefined){
             //   this.commonService.lgIssuanceApproval.status = pendingData.status;
             //  }

              if(res.userObject.responseCode !== null || res.userObject.responseCode !== undefined){
               this.commonService.lgIssuanceApproval.response_code = res.userObject.responseCode;
              }


             //  if(pendingData.terms_condition_message !== null || pendingData.terms_condition_message !== undefined){
             //   this.commonService.lgIssuanceApproval.terms_condition_message = pendingData.terms_condition_message;
             //   console.log("........................pendingData.terms_condition_message"+pendingData.terms_condition_message);
             //  }

             //  if(pendingData.request_id !== null || pendingData.request_id !== undefined){
             //   this.commonService.lgIssuanceApproval.request_id = pendingData.request_id;
             //   console.log("........................pendingData.request_id"+pendingData.request_id);
             //  }

             // this.router.navigate(['requestLgIssuanceApproval']);


             // //this.lgIssuance.
            //  if(this.loginId == res.userObject.create_by){
            //   this.router.navigate(['/requestLgIssuanceApproval']);
            // }
            // else if(this.loginId !== res.userObject.create_by){
            //   this.router.navigate(['/requestLgIssuanceGovApproval']);
            // }
            // else {
            //   alert(res.responseMessage);

            // }

           }
          if ((res.responseCode === 1) && (this.selectedWriteupType === 'NONSAMA')) {

            if(this.roleId !== null || this.roleId !== undefined){
              if(this.roleId == 1){
                this.router.navigate(['/requestLgIssuanceApproval']);
              }
              else if(this.roleId == 2){
                this.router.navigate(['/requestLgIssuanceGovApproval']);
              }
            }


            this.commonService.alertMsgforLGIssuancePayment = res.responseMessage;
          }
          else if ((res.responseCode === 1) && (this.selectedWriteupType === 'SAMA' || this.selectedWriteupType === undefined)) {

            if(this.roleId !== null || this.roleId !== undefined){
              if(this.roleId == 1){
                this.router.navigate(['/requestLgIssuanceApproval']);
              }
              else if(this.roleId == 2){
                this.router.navigate(['/requestLgIssuanceGovApproval']);
              }
            }
          }

        });
    }
    else {
      // this.msgs = [];
      // this.msgs.push({severity:'error', summary:'Error Message', detail:'Validation failed'});
      this.isOpenValidationAlert = true;
    }
  }

  resetForm() {
    if (this.submitLG.valid) {
      console.log("Form Submitted!");
      this.submitLG.reset();
    }
  }

  closeAlert() {
    this.isOpenValidationAlert = false;
  }
  styleObject(): Object {
    if (this.isEditTnC) {
      return { background: '#fff', border: '1px solid #c3c3c3' }
    }
    else {
      return { background: '#efefef', border: '1px solid #c3c3c3' }
    }

  }

}
