import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestLgIssuanceComponent } from './request-lg-issuance.component';

describe('RequestLgIssuanceComponent', () => {
  let component: RequestLgIssuanceComponent;
  let fixture: ComponentFixture<RequestLgIssuanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestLgIssuanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestLgIssuanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
