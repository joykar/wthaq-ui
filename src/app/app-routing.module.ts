import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivateEntityReg1Component } from './private-entity-reg1/private-entity-reg1.component';
import { PrivateEntityReg2Component } from './private-entity-reg2/private-entity-reg2.component';
import { PrivateEntityReg3Component } from './private-entity-reg3/private-entity-reg3.component';
import { PrivateEntityRegFinalComponent } from './private-entity-reg-final/private-entity-reg-final.component';
import { GovernmentEntityReg1Component } from './government-entity-reg1/government-entity-reg1.component';
import { GovernmentEntityReg2Component } from './government-entity-reg2/government-entity-reg2.component';
import { GovernmentEntityReg3Component } from './government-entity-reg3/government-entity-reg3.component';
import { LoginComponent } from './login/login.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { RequestLgIssuanceComponent } from './request-lg-issuance/request-lg-issuance.component';
import { RequestLgIssuanceApprovalComponent } from './request-lg-issuance-approval/request-lg-issuance-approval.component';
import { RequestLgIssuancePaymentComponent } from './request-lg-issuance-payment/request-lg-issuance-payment.component';
import { LandingComponent } from './landing/landing.component';
import { GovEntityDashboardComponent } from './gov-entity-dashboard/gov-entity-dashboard.component';
import { GovPendingActionComponent } from './gov-pending-action/gov-pending-action.component';
import { HelpComponent } from './help/help.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RequestLgIssuanceGovApprovalComponent } from './request-lg-issuance-gov-approval/request-lg-issuance-gov-approval.component';

const routes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'login', component: LoginComponent },
  { path: 'forgotPassword', component: ForgotPasswordComponent },
  { path: 'privateEntityReg', component: PrivateEntityReg1Component },
  { path: 'privateEntityReg2', component: PrivateEntityReg2Component },
  { path: 'privateEntityReg3', component: PrivateEntityReg3Component },
  { path: 'privateEntityRegFinal', component: PrivateEntityRegFinalComponent },
  { path: 'dashboard', component: DashBoardComponent },
  // { path: 'privateEntityReg/dashboard', component: DashBoardComponent },
  // { path: 'privateEntityReg2/dashboard', component: DashBoardComponent },
  // { path: 'privateEntityReg3/dashboard', component: DashBoardComponent },
  // { path: 'privateEntityRegFinal/dashboard', component: DashBoardComponent },
  { path: 'governmentEntityReg', component: GovernmentEntityReg1Component },
  { path: 'governmentEntityReg2', component: GovernmentEntityReg2Component },
  { path: 'governmentEntityReg3', component: GovernmentEntityReg3Component },
  { path: 'govDashboard', component: GovEntityDashboardComponent },
  { path: 'pendingAction', component: GovPendingActionComponent },
  // { path: 'governmentEntityReg/govDashboard', component: GovEntityDashboardComponent },
  // { path: 'governmentEntityReg2/govDashboard', component: GovEntityDashboardComponent },
  // { path: 'governmentEntityReg3/govDashboard', component: GovEntityDashboardComponent },
  // { path: 'governmentEntityReg/pendingAction', component: GovPendingActionComponent },
  // { path: 'governmentEntityReg2/pendingAction', component: GovPendingActionComponent },
  // { path: 'governmentEntityReg3/pendingAction', component: GovPendingActionComponent },
  // { path: 'privateEntityReg/requestLgIssuance', component: RequestLgIssuanceComponent },
  // { path: 'privateEntityReg2/requestLgIssuance', component: RequestLgIssuanceComponent },
  // { path: 'privateEntityReg3/requestLgIssuance', component: RequestLgIssuanceComponent },
  // { path: 'privateEntityRegFinal/requestLgIssuance', component: RequestLgIssuanceComponent },
  // { path: 'privateEntityReg/dashboard/', component: DashBoardComponent },
  { path: 'requestLgIssuance', component: RequestLgIssuanceComponent },
  { path: 'requestLgIssuanceApproval', component: RequestLgIssuanceApprovalComponent },
  { path: 'requestLgIssuanceGovApproval', component: RequestLgIssuanceGovApprovalComponent },
  { path: 'requestLgIssuancePayment', component: RequestLgIssuancePaymentComponent },
  // { path: 'dashboard/requestLgIssuance', component: RequestLgIssuanceComponent },
  // { path: 'dashboard/requestLgIssuanceApproval', component: RequestLgIssuanceApprovalComponent },
  // { path: 'dashboard/requestLgIssuancePayment', component: RequestLgIssuancePaymentComponent },
  // { path: 'requestLgIssuance/dashboard', component: DashBoardComponent },
  // { path: 'requestLgIssuanceApproval/dashboard', component: DashBoardComponent },
  // { path: 'requestLgIssuancePayment/dashboard', component: DashBoardComponent },
  { path: 'help', component: HelpComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
