import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovernmentEntityReg1Component } from './government-entity-reg1.component';

describe('GovernmentEntityReg1Component', () => {
  let component: GovernmentEntityReg1Component;
  let fixture: ComponentFixture<GovernmentEntityReg1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovernmentEntityReg1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernmentEntityReg1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
