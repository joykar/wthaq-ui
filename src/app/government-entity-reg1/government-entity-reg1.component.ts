import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-government-entity-reg1',
  templateUrl: './government-entity-reg1.component.html',
  styleUrls: ['./government-entity-reg1.component.css']
})
export class GovernmentEntityReg1Component implements OnInit {

  submitGovReg1: FormGroup;
  theCheckbox = false;
  isExpandDiv: boolean;
  checkboxTermsnCond: boolean = false;
  isDisableContinue: boolean = true;
  entityList: any[] = [];
  //entityUID: any;
  entityTnC: string;
  entity: any;


  noPattern: string = '^(0|[1-9][0-9]*)$';
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  textPattern = "^[A-Za-z ]*$";

  validation_messages = {
    entity: [
      { type: "required", message: "This is a required field." }
    ],
    entityUID: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Entity UID should be composed of digits."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 10."
      }
    ],
    entityname: [
      { type: "required", message: "This is a required field." },
      // {
      //   type: "pattern",
      //   message: "Entity name should be composed of characters."
      // },
      // {
      //   type: "maxlength",
      //   message: "Maximum length should be 20."
      // }

    ],
    name: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Name should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ],
    entityNationalID: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "National Id should be composed of digits."
      },
      {
        type: "maxlength",
        message: "You can enter national Id of maximum 9 digits."
      }
    ],
    entityMobNo: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Mobile number should be composed of digits."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 10."
      },
      {
        type: "minlength",
        message: "Minimum length should be 10."
      }
    ],
    entityEmail: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Please enter a valid emailid."
      }
    ]

  }


  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService,
    private messageService: MessageService) {

  }

  toggleVisibility(e) {

    console.log("this.theCheckbox::::" + e);
    if (e === true) {
      this.isExpandDiv = true;
      this.theCheckbox = true;
      this.submitGovReg1.controls['entity'].setValue('');
      this.submitGovReg1.controls['UID'].setValue(null);
      this.entityTnC = "y";
    }
    else {
      this.isExpandDiv = false;
      this.theCheckbox = false;
      this.entityTnC = "n";
    }
  }

  onCheckedTerms(checkboxTermsnCond) {
    console.log("checkboxTermsnCond:::" + checkboxTermsnCond);
    if (checkboxTermsnCond === true) {
      //this.entityTnC = "y";
      this.isDisableContinue = false;
    }
    else {
      this.isDisableContinue = true;
      //this.entityTnC = "n";
    }
  }

  ngOnInit() {
    this.submitGovReg1 = this.formBuilder.group({
      UID: [
        ""
      ],
      checkedGovEntity: [
        ""
      ],
      entityTnC: [
        ""
      ],
      entity: [
        "",
        [
          Validators.required,
        ]
      ],
      entityUID: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(10)
        ]
      ],
      entityname: [
        "",
        [
          Validators.required,
          //Validators.pattern(this.noPattern),
          //Validators.maxLength(20)
        ]
      ],
      name: [
        "",
        [
          Validators.required,
          //Validators.pattern(this.textPattern),
          Validators.maxLength(20)
        ]
      ],
      entityNationalID: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(9)
        ]
      ],
      entityMobNo: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(10),
          Validators.minLength(10)
        ]
      ],
      entityEmail: [
        "",
        [
          Validators.required,
          Validators.pattern(this.emailPattern)
        ]
      ]

    })

    console.log("@@@@@" + JSON.stringify(this.commonService.govEntityDEtails));
    if (this.commonService.govEntityDEtails !== undefined || this.commonService.govEntityDEtails !== null) {
      if (this.commonService.govEntityDEtails.checkedGovEntity === true) {
        if (this.commonService.govEntityDEtails.entityname !== undefined || this.commonService.govEntityDEtails.entityname !== null) {
          this.submitGovReg1.controls["entityname"].setValue(this.commonService.govEntityDEtails.entityname);
        }
        if (this.commonService.govEntityDEtails.entityUID !== undefined || this.commonService.govEntityDEtails.entityUID !== null) {
          this.submitGovReg1.controls["entityUID"].setValue(this.commonService.govEntityDEtails.entityUID);
        }
        this.submitGovReg1.controls["checkedGovEntity"].setValue(true);
      }
      else {
        if (this.commonService.govEntityDEtails.entityname !== undefined || this.commonService.govEntityDEtails.entityname !== null) {
          //this.submitGovReg1.controls["entity"].setValue(this.commonService.govEntityDEtails.entityname);
          this.getEntity();
          for (let entity of this.entityList) {
            if (entity[1] === this.commonService.govEntityDEtails.entityname) {
              console.log("Entity Id is" + entity[2]);
              this.submitGovReg1.controls["entity"].setValue(entity);
              this.entity = entity[1];
              break;
            }
          }

        }
        if (this.commonService.govEntityDEtails.entityUID !== undefined || this.commonService.govEntityDEtails.entityUID !== null) {
          this.submitGovReg1.controls["UID"].setValue(this.commonService.govEntityDEtails.entityUID);
        }
        this.submitGovReg1.controls["checkedGovEntity"].setValue(false);
      }
      if (this.commonService.govEntityDEtails.name !== undefined || this.commonService.govEntityDEtails.name !== null) {
        this.submitGovReg1.controls["name"].setValue(this.commonService.govEntityDEtails.name);
      }
      if (this.commonService.govEntityDEtails.entityNationalID !== undefined || this.commonService.govEntityDEtails.entityNationalID !== null) {
        this.submitGovReg1.controls["entityNationalID"].setValue(this.commonService.govEntityDEtails.entityNationalID);
      }
      if (this.commonService.govEntityDEtails.entityMobNo !== undefined || this.commonService.govEntityDEtails.entityMobNo !== null) {
        this.submitGovReg1.controls["entityMobNo"].setValue(this.commonService.govEntityDEtails.entityMobNo);
      }
      if (this.commonService.govEntityDEtails.entityEmail !== undefined || this.commonService.govEntityDEtails.entityEmail !== null) {
        this.submitGovReg1.controls["entityEmail"].setValue(this.commonService.govEntityDEtails.entityEmail);
      }

      if (this.commonService.govEntityDEtails.entityTnC === 'y') {
        this.submitGovReg1.controls["entityTnC"].setValue(true);
      }
      else {
        this.submitGovReg1.controls["entityTnC"].setValue(false);
      }



    }
    else {
      this.submitGovReg1.controls["entityname"].setValue(null);
      this.submitGovReg1.controls["entityUID"].setValue(null);
      this.submitGovReg1.controls["checkedGovEntity"].setValue(false);
      this.submitGovReg1.controls["entity"].setValue(null);
      this.submitGovReg1.controls["UID"].setValue(null);
      this.submitGovReg1.controls["name"].setValue(null);
      this.submitGovReg1.controls["entityNationalID"].setValue(null);
      this.submitGovReg1.controls["entityMobNo"].setValue(null);
      this.submitGovReg1.controls["entityEmail"].setValue(null);

    }

    this.getEntity();

  }
  getEntity() {
    this.commonService.getEntityList()
      .subscribe(res => {
        console.log("Entity list::::" + JSON.stringify(res));
        this.entityList = res;
        console.log("Entity list is::::" + JSON.stringify(this.entityList))

      });
  }

  onBlur(entityname) {
    console.log("entityname:::::" + entityname);
    this.checkBenName(entityname);
  }

  checkBenName(entityname) {
    this.commonService.checkBenName(entityname)
      .subscribe(res => {
        console.log("Response for entityname is::::" + JSON.stringify(res));
        if (res.responseCode === 0) {
          alert(res.responseMessage);
          this.submitGovReg1.controls["entityname"].setValue(null);
        }

      })
  }

  onClick(e: any) {
    console.log('entity name' + e);
    for (let entity of this.entityList) {
      if (entity[1] === e) {
        console.log("Entity Id is" + entity[2]);
        //this.entityUID = entity[0];
        this.submitGovReg1.controls['UID'].setValue(entity[2]);
        break;
      }
    }
  }

  submitGovRegData() {
    console.log(JSON.stringify(this.submitGovReg1.value));
    if (this.theCheckbox === false) {
      this.submitGovReg1.controls["entityname"].setValue("abc");
      this.submitGovReg1.controls["entityUID"].setValue("123");
    }
    else{
      this.submitGovReg1.controls["entity"].setValue("abc");
      this.submitGovReg1.controls["UID"].setValue("123");
    }

    if (this.submitGovReg1.valid) {
      if (this.theCheckbox === true) {
        this.commonService.govEntityDEtails.entityUID = this.submitGovReg1.controls['entityUID'].value;
        this.commonService.govEntityDEtails.entityname = this.submitGovReg1.controls['entityname'].value;
        this.commonService.govEntityDEtails.checkedGovEntity = true;
      }
      else {
        this.commonService.govEntityDEtails.entityUID = this.submitGovReg1.controls['UID'].value;
        this.commonService.govEntityDEtails.entityname = this.submitGovReg1.controls['entity'].value;
        this.commonService.govEntityDEtails.checkedGovEntity = false;
      }

      //this.commonService.govEntityDEtails.UID = this.submitGovReg1.controls['UID'].value;
      if (this.entityTnC === undefined) {
        this.commonService.govEntityDEtails.entityTnC = 'n';
      }
      else {
        this.commonService.govEntityDEtails.entityTnC = this.entityTnC;
      }

      this.commonService.govEntityDEtails.name = this.submitGovReg1.controls['name'].value;
      this.commonService.govEntityDEtails.entityNationalID = this.submitGovReg1.controls['entityNationalID'].value;
      this.commonService.govEntityDEtails.entityMobNo = this.submitGovReg1.controls['entityMobNo'].value;
      this.commonService.govEntityDEtails.entityEmail = this.submitGovReg1.controls['entityEmail'].value;
      console.log("this.commonService.govEntityDEtails:::::" + JSON.stringify(this.commonService.govEntityDEtails));
      this.commonService.isGovPart1Completed = true;
      this.router.navigate(['governmentEntityReg2']);
    }
  }

  showError() {
    this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Validation failed' });
  }

  resetForm() {
    if (this.submitGovReg1.valid) {
      console.log("Form Submitted!");
      this.submitGovReg1.reset();
    }
  }


}
