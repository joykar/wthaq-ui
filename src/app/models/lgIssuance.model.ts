export class LGIssuanceModel {

  LGType: string;
  writeUpType: string;
  bankCode: string;
  beneficiaryId: string;
  //beneficiaryId: number;
  IBAN: string;
  CR: string;
  Amount: string;
  amountInNumber: string;
  currency: string;
  LGStartDate: string;
  LGEndDate: string;
  projectName: string;
  projectNumber: string;
  ZakatPeriodStartDate: string;
  ZakatPeriodEndDate: string;
  puroseOfBond: string;
  bayanNuymber: string;
  checkBox: string;
  privateEntityCheckBox: boolean;
  bankName: string;
  requestId: string;
  termsNconditionsMessage: string;
  beneficiary_name: string;
  project_name: string;
  request_date: string;
  status: string;
  response_code: string;
  terms_condition_message: string;
  request_id: any;
  create_by: any;
  ninNumber: any;


}