export interface UserDetailsModel{
  userId: string;
  password: string;
  otp: string;
  mobileNo: string;
  commRegNo: string;
  EntityName: string;
  ContactInformation: string;
  Address1: string;
  Address2: string;
  State: string;
  PINCode: string;
  Country: string;
  IBANNumber: string;
  BankName: string;

  RepresentativeName: string;
  RepresentativeID: string;
  OfficeEMail: string;
  OfficeMobileNumber: string;
  name: string;
  userName: string;

  crFile: string;
  rniFile: string;
  lalFile: string;
  laglFile: string;

  crFilePath: string;
  rniFilePath: string;
  lalFilePath: string;
  laglFilePath: string;

  crFileContent: any;
  rniFileContent: any;
  lalFileContent: any;
  laglFileContent: any;

  Email: string;
  NationalId: string;
  Mobile: string;
  commRegNoMCI: string;

}
