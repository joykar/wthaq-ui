
export class GovernmentEntityModel {

  UID: any;
  //checkedGovEntity: boolean;
  //termsAndCond: boolean;
  entity: string;
  entityUID: string;
  entityname: string;
  name: string;
  entityNationalID: string;
  entityMobNo: string;
  entityEmail: string;
  address1: string;
  address2: string;
  country: string;
  city: string;
  lalFile: any;
  rniFile: any;
  laglFile: any;
  lalFileName: any;
  rniFileName: any;
  laglFileName: any;
  userId: string;
  entityTnC: string;
  checkedGovEntity: boolean;
}
