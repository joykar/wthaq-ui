import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {


  submitForgotPassword: FormGroup;
  textPattern = "^[A-Za-z ]*$";
  isOpenAlert: boolean = false;
  alertMsg: string = '';

  validation_messages = {
    userIdForgotPass: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "User Id should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ]
    // mobileNo: [
    //   { type: "required", message: "This is a required field." }
    // ],
    // otp: [
    //   { type: "required", message: "This is a required field." }
    // ]
  }

  constructor(private formBuilder: FormBuilder, private commonService: CommonService, private router: Router) { }

  ngOnInit() {
    this.submitForgotPassword = this.formBuilder.group({
      userIdForgotPass: [
        "",
        [
          Validators.required,
          Validators.pattern(this.textPattern),
          Validators.maxLength(20)
        ]
      ]
      // mobileNo: [
      //   "",
      //   [
      //     Validators.required
      //   ]
      // ],
      // otp: [
      //   "",
      //   [
      //     Validators.required
      //   ]
      // ]

    })
  }

  onSubmit(){
    console.log("Forgot password data:::" + JSON.stringify(this.submitForgotPassword.value));
    if (this.submitForgotPassword.valid) {
      console.log("Forgot password data:::");
      this.commonService.postForgotPasswordDetails(this.submitForgotPassword.value.userIdForgotPass)
        .subscribe(res => {
          console.log("Forgot password Response is::::" + JSON.stringify(res));
          if(res.responseCode === 1){
            this.isOpenAlert = true;
            this.alertMsg = res.responseMessage;
            this.submitForgotPassword.reset();
          }
        });
      }

  }
  resetForm(){
    if (this.submitForgotPassword.valid) {
      console.log("Form Submitted!");
      this.submitForgotPassword.reset();
    }
  }

}
