import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserDetailsModel } from '../models/userDetails.model';
import { MCIModel } from '../models/mci.model';
import {GovernmentEntityModel} from '../models/govEntity.model';
import {LGIssuanceModel} from '../models/lgIssuance.model';

import { from } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  alertMsgforRegistrationFinal: string;
  responseCodeforRegistrationFinal: number;
  alertMsgforLGIssuancePayment: string;
  alertMsgforGovLGApproval: string = "";
  alertMsgforPrivateLGApproval: string = "";
  responseCode: number;

  logoutStatus:number=0;


  public hostUrl: string = 'http://182.73.216.91:8080';

  public mciUrl: string = "/mciData ";
  public saveUserUrl: string = "/save-user ";
  public loginUrl: string = "/LogInUser ";
  public bankListUrl: string = "/selectBankName ";
  public ibanVerifyUrl: string = "/ibanVerify ";
  public selectGovermentEntityUrl: string = "/selectGovermentEntity ";
  public govtEntitySaveUrl: string = "/govtEntitySave ";
  public selectLGTypeUrl: string = "/selectLGType ";
  public selectCurrencyUrl: string = "/selectCurrency ";
  public selectIBANUrl: string = "/selectIBAN/ ";
  public lgIssueDataSubmitUrl: string = "/lgIssueDataSubmit/ ";
  public lgIssueSubmitUrl: string = "/lgIssueDataSubmit/ ";
  public getAllLgRequestBeneficiarySpecificUrl: string = "/getAllLgRequestBeneficiarySpecific/ ";
  public selectGovermentEntityRegisteredUrl: string = "/selectGovermentEntityRegistered/ ";
  public changeStatusOfLgRequestBeneficiaryUrl: string = "/changeStatusOfLgRequestBeneficiary/ ";
  public forgotPasswordUrl: string = "/forgotPassword/ ";
  public checkSupplierCrnUrl: string = "/CheckSupplierCrn/ ";
  public checkValidLoginIdUrl: string = "/CheckValidLoginId/ ";
  public checkBenNameUrl: string = "/checkBenName/ ";
  public selectWriteUpTypeUrl: string = "/selectWriteUpType/ ";
  public cancellingLgRequestFromSupplierUrl: string = "/CancellingLgRequestFromSupplier/ ";
  public acceptingLgRequestFromSupplierUrl: string = "/AcceptingLgRequestFromSupplier/ ";








  userDetails: any;
  govEntityDEtails: GovernmentEntityModel = new GovernmentEntityModel();
  lgIssuanceApproval: LGIssuanceModel;
  isPrivatePart1Completed: boolean = false;
  isPrivatePart2Completed: boolean = false;
  isPrivatePart3Completed: boolean = false;
  isGovPart1Completed: boolean = false;
  isGovPart2Completed: boolean = false;
  isGovPart3Completed: boolean = false;
  isPrivateLgPending1Completed: boolean = false;
  isEnablePrivateLGApprovalbuttons: string ;

 tokenParam={'WTHAQ_AUTH_TOKEN':''};

  headers = new HttpHeaders();


  constructor(private http: HttpClient,   private router: Router) { }

  //post MCI data
  postMCIDetails(body: MCIModel): Observable<UserDetailsModel> {
    let params = new FormData();
    params.append('crn', body.crn);
    params.append('mno', body.mno);
    return this.http.post<UserDetailsModel>(this.hostUrl+this.mciUrl, params)
  }

  //Post private Registration details
  postRegistrationDetails(body: any): Observable<any> {
    let params = new FormData();
    params.append('rniFileContent', body.rniFile);
    params.append('crFileContent', body.crFile);
    params.append('lalFileContent', body.lalFile);
    params.append('laglFileContent', body.laglFile);
    params.append('name', body.name);
    params.append('userName', body.userName);
    params.append('Email', body.email);
    params.append('userId', body.userId);
    params.append('NationalId', body.nationalId);
    params.append('ContactInformation', body.mobileNo);
    params.append('EntityName', body.entityName);
    params.append('commRegNoMCI', body.crNumber);
    params.append('mobileNo', body.mobileNo);
    params.append('OfficeEMail', body.emailAddress);
    params.append('Address2', body.address2);
    params.append('Address1', body.address1);
    params.append('commRegNo', body.crNumber);
    params.append('OfficeMobileNumber', body.mobileNo);
    params.append('crn', body.crn);
    params.append('mno', body.mno);
    return this.http.post<UserDetailsModel>(this.hostUrl+this.saveUserUrl, params)
  }

  //Login
  postLoginDetails(body: any): Observable<any> {
    let params = new FormData();
    params.append('userId', body.userId);
    params.append('password', body.password);
    return this.http.post<any>(this.hostUrl+this.loginUrl, params)
  }

  //Get bank list
  getBankNameList():Observable<any>{
    return this.http.get<any>(this.hostUrl+this.bankListUrl)
  }

  //Post bank details
  postBankDetails(body: any, userId: string): Observable<any> {
    let params = new FormData();
    params.append('bi', body.bi);
    params.append('bankName', body.bankName);
    params.append('userId', userId);
    return this.http.post<any>(this.hostUrl+this.ibanVerifyUrl, params)
  }

//Entity list
  getEntityList():Observable<any>{
    return this.http.get<any>(this.hostUrl+this.selectGovermentEntityUrl)
  }

  //save gov entity
  postGovEntityDetails(body: GovernmentEntityModel): Observable<any> {
    let params = new FormData();
    ///params.append('entity', body.UID);
    params.append('entityname', body.entityname);
    params.append('entityUID', body.entityUID);
    params.append('name', body.name);
    params.append('entityNationalID', body.entityNationalID);
    params.append('entityMobNo', body.entityMobNo);
    params.append('entityEmail', body.entityEmail);
    params.append('entityTnC', body.entityTnC);
    params.append('address1', body.address1);
    params.append('address2', body.address2);
    params.append('country', body.country);
    params.append('city', body.city);
    params.append('lalFile', body.lalFile);
    params.append('laglFile', body.laglFile);
    params.append('rniFile', body.rniFile);
    params.append('userId', body.userId);

    return this.http.post<any>(this.hostUrl+this.govtEntitySaveUrl, params)
  }

  //Get Lg type list
  getLGTypeList():Observable<any>{
    return this.http.get<any>(this.hostUrl+this.selectLGTypeUrl)
  }

  //Get Currency type list
  getCurrencyList():Observable<any>{
    return this.http.get<any>(this.hostUrl+this.selectCurrencyUrl)
  }

  //Get writeup list
  getWriteUpList():Observable<any>{
    return this.http.get<any>(this.hostUrl+this.selectWriteUpTypeUrl)
  }

  //get IBAN list
  getIBANList(id: string):Observable<any>{
    // let params = new FormData();
    // params.append('id', userId);
    return this.http.get<any>(`http://182.73.216.91:8080/selectIBAN/${id}`)
  }

  //post LGIssuance Details
  postLGIssuanceDetails(body: LGIssuanceModel, userId: string): Observable<any> {
    console.log("Submitted data::::"+JSON.stringify(body));
    console.log("userId:::"+userId);
    let params = new FormData();
    params.append('LGType', body.LGType);
    params.append('writeUpType', body.writeUpType);
    params.append('bankCode', body.bankCode);
    params.append('IBAN', body.IBAN);
    params.append('beneficiaryId', body.beneficiaryId);
    params.append('CR', body.CR);
    params.append('Amount', body.Amount);
    params.append('amountInNumber', body.amountInNumber);
    params.append('currency', body.currency);
    params.append('LGStartDate', body.LGStartDate);
    params.append('LGEndDate', body.LGEndDate);
    params.append('projectName', body.projectName);
    params.append('projectNumber', body.projectNumber);
    params.append('ZakatPeriodStartDate', body.ZakatPeriodStartDate);
    params.append('ZakatPeriodEndDate', body.ZakatPeriodEndDate);
    params.append('puroseOfBond', body.puroseOfBond);
    params.append('bayanNuymber', body.bayanNuymber);
    params.append('checkBox', body.checkBox);
    params.append('termsNconditionsMessage', body.termsNconditionsMessage);
    params.append('ninNumber', body.ninNumber);
    if(body.privateEntityCheckBox){
      params.append('privateEntityCheckBox', 'true');
    }
    else{
      params.append('privateEntityCheckBox', 'false');
    }

    params.append('userId', userId);


    return this.http.post<any>(this.hostUrl+this.lgIssueSubmitUrl, params)
  }

  getAllLGRequestBeneficiarySpecific(benId : string): Observable<any>{

    //return this.http.get<any>(`http://192.168.1.75:8080/getAllLgRequestBeneficiarySpecific/${benId }`)
    return this.http.get<any>(`http://182.73.216.91:8080/getAllLgRequestBeneficiarySpecific/${benId }`)
  }


  getAllLGRequestBeneficiarySpecificPri(benId : string): Observable<any>{
    return this.http.get<any>(`http://182.73.216.91:8080/getAllLgRequestSupplierSpecific/${benId }`)
  }


  getAllLGRequestBeneficiarySpecificPendingPri(benId : string): Observable<any>{
    return this.http.get<any>(`http://182.73.216.91:8080/getAllLgRequestSupplierSpecific/${benId }`)
  }



  getSelectGovermentEntityRegistered(): Observable<any>{
    return this.http.get<any>(this.hostUrl+this.selectGovermentEntityRegisteredUrl)
  }

  //post LGIssuance Approval Details
  postChangeedTnCOfLgRequestBeneficiary(requestId: string, termsNconditions: string): Observable<any> {
    console.log("requestId::::"+requestId);
    console.log("termsNconditions:::"+termsNconditions);
    let params = new FormData();
    params.append('requestId', requestId);
    params.append('termsNconditions', termsNconditions);

    return this.http.post<any>(this.hostUrl+this.changeStatusOfLgRequestBeneficiaryUrl, params)
  }


    //post LGIssuance Approval Details
    postTnCOfLgRequestBeneficiary(requestId: string,): Observable<any> {
      console.log("requestId::::"+requestId);
      let params = new FormData();
      params.append('requestId', requestId);

      return this.http.post<any>(this.hostUrl+this.changeStatusOfLgRequestBeneficiaryUrl, params)
    }

    //forgot password
    postForgotPasswordDetails(userIdForgotPass: string,): Observable<any> {
      console.log("userIdForgotPass::::"+userIdForgotPass);
      let params = new FormData();
      params.append('userIdForgotPass', userIdForgotPass);
      //return this.http.get<any>(`http://192.168.1.75:8080/forgotPassword/${userIdForgotPass }`)
      return this.http.post<any>(this.hostUrl+this.forgotPasswordUrl, params)
    }

    checkSupplierCrn(crnno){
      let params = new FormData();
      params.append('crnno', crnno);
      return this.http.post<any>(this.hostUrl+this.checkSupplierCrnUrl, params)
    }

    checkSupplierCR(crn){
      return this.http.get<any>(`http://182.73.216.91:8080/mciGetBenName/${crn }`)
    }


    checkValidLoginId(tempLoginId){
      let params = new FormData();
      params.append('tempLoginId', tempLoginId);
      return this.http.post<any>(this.hostUrl+this.checkValidLoginIdUrl, params)
    }

    checkBenName(benName){
      let params = new FormData();
      params.append('benName', benName);
      return this.http.post<any>(this.hostUrl+this.checkBenNameUrl, params)
    }

    getSelectedCRN(loginid){
      return this.http.get<any>(`http://182.73.216.91:8080//selectCRN/${loginid }`)
    }

    CancellingLgRequestFromSupplier(requestId){
      let params = new FormData();
      params.append('requestId', requestId);
      return this.http.post<any>(this.hostUrl+this.cancellingLgRequestFromSupplierUrl, params)
    }

    AcceptingLgRequestFromSupplier(requestId){
      let params = new FormData();
      params.append('requestId', requestId);
      return this.http.post<any>(this.hostUrl+this.acceptingLgRequestFromSupplierUrl, params)
    }



/*  Tajel  */



    logout(loginid){
      let params = new FormData();
      params.append('loginId', loginid);
     this.http.post<string>(this.hostUrl+`//logout`,params ,  this.createJWTTokenHeader() ).subscribe(res => {});
     localStorage.clear();
     this.router.navigate(['login']);
     return  ;

    }

    

    populateTableOfCRNByNID(loginid){

      this.checkLocalStorageForLogout();

      let params = new FormData();
      params.append('loginId', loginid);
      return this.http.post<any>(this.hostUrl+`//getCRNDetailsByNID`, params ,  this.createJWTTokenHeader() )
    }

    otpByLoginId(loginid)
    {
    //  alert(" otpByLoginId "+loginid)
      this.checkLocalStorageForLogout();

      let params = new FormData();
      params.append('loginId', loginid);

      return this.http.post<any>(this.hostUrl+`//getOtpByloginId`, params ,  this.createJWTTokenHeader() )
    }

checkSessionExpired(loginid)
{
 
  this.checkLocalStorageForLogout();
   
  let params = new FormData();
  params.append('loginId', loginid);

/******************************************* */

return new Promise(resolve=>{
  this.http.post<any>(this.hostUrl+`//checkSessionExpired`, params ,  this.createJWTTokenHeader()  )
   
   .subscribe(
      (data:any) => {
          console.log(data);
         // data=0;
          resolve(data);
   })
});



/*********************************************** */
/*
  
  .subscribe(res => {
  
  alert("checkSessionExpired logout "+res);
  
  if(res==0)
  {
   
  this.logoutStatus=0;
  this.logout(loginid);
}
else{
  this.logoutStatus=1;
 }     

 alert("logoutStatus checkSessionExpired "+this.logoutStatus);
 return this.logoutStatus;

  })



*/
 // if(logout==0){
 //  this.logout(loginid);
 // }



}


checkLocalStorageForLogout(){
  if (localStorage.getItem('jwtToken') !== undefined  && localStorage.getItem('jwtToken') !== null) {
return;
  }
else{
localStorage.clear();
this.router.navigateByUrl('/login');
return;
}
}


setTokenHeaderValueFromStorage(){
 // alert("token: "+localStorage.getItem('jwtToken') );
  this.tokenParam.WTHAQ_AUTH_TOKEN=localStorage.getItem('jwtToken') ;
}
getTokenHeaderValueFromStorage(){
  this.setTokenHeaderValueFromStorage();
  return this.tokenParam.WTHAQ_AUTH_TOKEN;
}

setTokenHeaderFromStorage(){
  //this.setTokenHeaderValueFromStorage();
  alert("SET: "+this.getTokenHeaderValueFromStorage());
  this.headers.append('WTHAQ_AUTH_TOKEN', this.getTokenHeaderValueFromStorage())  ;
}
getTokenHeaderFromStorage(){
  this.setTokenHeaderFromStorage();
 // alert("GET: "+this.headers.has('WTHAQ_AUTH_TOKEN'));
  return  this.headers ;
}


createJWTTokenHeader(){
let  httpOptions = {
    headers: new HttpHeaders({
      'WTHAQ-AUTH-TOKEN': ''+ localStorage.getItem('jwtToken')  /*this.getTokenHeaderValueFromStorage()*/
    })
  }

//  alert("Token: "+localStorage.getItem('jwtToken'));

//  let headers = new HttpHeaders();
//  headers= headers.append('WTHAQ_AUTH_TOKEN', this.getTokenHeaderValueFromStorage())  ;
 // alert("GET: "+headers.has('WTHAQ_AUTH_TOKEN'));
return httpOptions;
}


/*  Tajel  */






}
