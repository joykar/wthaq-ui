import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateEntityReg1Component } from './private-entity-reg1.component';

describe('PrivateEntityReg1Component', () => {
  let component: PrivateEntityReg1Component;
  let fixture: ComponentFixture<PrivateEntityReg1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateEntityReg1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateEntityReg1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
