import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {CommonService} from '../services/common-service.service';
import {MCIModel} from '../models/mci.model';

@Component({
  selector: 'app-private-entity-reg1',
  templateUrl: './private-entity-reg1.component.html',
  styleUrls: ['./private-entity-reg1.component.css']
})
export class PrivateEntityReg1Component implements OnInit {

  submitReg1: FormGroup;
  mciModel: MCIModel;
  isOpenValidationAlert: boolean = false;
  crNumber: string;
  contactNo: string
  dateOfBirth: Date;
  maxDate: Date;

  noPattern: string = '^(0|[1-9][0-9]*)$';
  textPattern = "^[A-Za-z ]*$";

  validation_messages = {
    crn: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Commercial registration number should be numeric."
      },
      {
        type: "maxlength",
        message: "Commercial registration number should be 15 digits."
      },
      {
        type: "minlength",
        message: "Commercial registration number should be 15 digits."
      }
    ],
    mno: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "National identification number should be composed of digits."
      },
      {
        type: "maxlength",
        message: "National identification number should be 10 digits."
      },
      {
        type: "minlength",
        message: "National identification number should be 10 digits."
      }
    ],
    dateOfBirth: [
      { type: "required", message: "This is a required field." }
    ],
  }

  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService) {

  }

  ngOnInit() {

    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    this.maxDate = new Date();
    this.maxDate.setMonth(month);
    this.maxDate.setFullYear(year);

    this.submitReg1 = this.formBuilder.group({
      crn: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(15),
          Validators.minLength(15)
        ]
      ],
      mno: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(10),
          Validators.minLength(10)
        ]
      ],
      dateOfBirth: [
            null,
            [
              Validators.required

            ]
          ]

    })

    // console.log("@@@@@"+JSON.stringify(this.commonService.userDetails));
    // if(this.commonService.userDetails !== undefined || this.commonService.userDetails !== null){
    //   if(this.commonService.userDetails.crNumber !== undefined || this.commonService.userDetails.crNumber !== null){
    //     this.submitReg1.controls["crn"].setValue(this.commonService.userDetails.crNumber);
    //   }
    //   if(this.commonService.userDetails.contactNo !== undefined || this.commonService.userDetails.contactNo !== null){
    //     this.submitReg1.controls["mno"].setValue(this.commonService.userDetails.contactNo);
    //   }
    //   if(this.commonService.userDetails.dateOfBirth !== undefined || this.commonService.userDetails.dateOfBirth !== null){
    //     this.submitReg1.controls["dateOfBirth"].setValue(this.commonService.userDetails.dateOfBirth);
    //   }

    //   // this.submitReg1.controls["crn"].setValue(this.commonService.userDetails.crn);
    //   // this.submitReg1.controls["mno"].setValue(this.commonService.userDetails.mno);
    // }
    // else{
    //   this.submitReg1.controls["crn"].setValue(null);
    //   this.submitReg1.controls["mno"].setValue(null);
    //   this.submitReg1.controls["dateOfBirth"].setValue(null);
    // }


  }

  onBlur(crn){
    console.log("crn:::::"+crn);
    this.checkSupplierCrn(crn);
  }

  checkSupplierCrn(crn){
    this.commonService.checkSupplierCrn(crn)
      .subscribe(res => {
        console.log("Response for CRN is::::"+JSON.stringify(res));
        if(res.responseCode === 0){
          alert(res.responseMessage);
          this.submitReg1.controls["crn"].setValue(null);
        }

      })
  }

  gotoReg2Page(){
    console.log("Reg 1 data:::::"+JSON.stringify(this.submitReg1.value));
    if(this.submitReg1.valid){
      this.mciModel = this.submitReg1.value;
      this.commonService.postMCIDetails(this.mciModel)
      .subscribe(res => {
        console.log("Response is::::"+JSON.stringify(res));
        this.commonService.userDetails = res;

        //this.router.navigate(['/privateEntityReg2', { state: {mciResponseData: res}}]);
        this.commonService.isPrivatePart1Completed = true;
        this.router.navigate(['/privateEntityReg2']);
      })
    }
    else{
      this.isOpenValidationAlert = true;
    }

  }

  resetForm(){
    if (this.submitReg1.valid) {
      console.log("Form Submitted!");
      this.submitReg1.reset();
    }
  }

  closeAlert(){
    this.isOpenValidationAlert = false;
  }


}
