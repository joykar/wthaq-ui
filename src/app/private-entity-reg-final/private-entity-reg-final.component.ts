import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common-service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-private-entity-reg-final',
  templateUrl: './private-entity-reg-final.component.html',
  styleUrls: ['./private-entity-reg-final.component.css']
})
export class PrivateEntityRegFinalComponent implements OnInit {

  submitBankDtl: FormGroup;
  isShowAlertDiv: boolean;
  isDisabledSubmitButton: boolean;
  bankList: any[] = [];
  userId: string;
  isOpenValidationAlert: boolean = false;

  constructor(private commonService: CommonService, private formBuilder: FormBuilder, private router: Router) {
    if(this.commonService.responseCodeforRegistrationFinal != undefined){
      //if(this.commonService.responseCodeforRegistrationFinal === 1 || this.commonService.responseCodeforRegistrationFinal === 100 || this.commonService.responseCodeforRegistrationFinal === 104 || this.commonService.responseCodeforRegistrationFinal === 106){
        if(this.commonService.responseCodeforRegistrationFinal === 1 || this.commonService.responseCodeforRegistrationFinal === 100 || this.commonService.responseCodeforRegistrationFinal === 106){
        this.isDisabledSubmitButton = true;
      }
      else if(this.commonService.responseCodeforRegistrationFinal === 101){
        this.isDisabledSubmitButton = false;
      }
    }
    console.log(this.commonService.alertMsgforRegistrationFinal);
    if(this.commonService.alertMsgforRegistrationFinal != undefined){
      this.isShowAlertDiv = true;
    }
    else{
      this.isShowAlertDiv = false;
    }
   }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    console.log("this.userId::::"+this.userId);

    this.submitBankDtl = this.formBuilder.group({
      bi: [
        "",
        [
          Validators.required
        ]
      ],
      bankName: [
        "",
        [
          Validators.required
        ]
      ]

    })

    this.commonService.getBankNameList()
    .subscribe(res => {
      console.log("Bank name list::::"+JSON.stringify(res));
      this.bankList = res;
      //console.log("Bank list is::::"+JSON.stringify(this.bankList));
    });
  }

  postBankDetails(){
    console.log("Bank details::::"+JSON.stringify(this.submitBankDtl.value));
    if(this.submitBankDtl.valid){
      this.commonService.postBankDetails(this.submitBankDtl.value, this.userId)
      .subscribe(res => {
        console.log("Reg final page Response is::::"+JSON.stringify(res));
        if(res.responseCode === 1){
          this.router.navigate(['/login']);
        }

      });
    }
    else{
      this.isOpenValidationAlert = true;
    }
  }

  resetForm(){
    if (this.submitBankDtl.valid) {
      console.log("Form Submitted!");
      this.submitBankDtl.reset();
    }
  }

  closeAlert(){
    this.isOpenValidationAlert = false;
  }

}
