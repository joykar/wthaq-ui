import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateEntityRegFinalComponent } from './private-entity-reg-final.component';

describe('PrivateEntityRegFinalComponent', () => {
  let component: PrivateEntityRegFinalComponent;
  let fixture: ComponentFixture<PrivateEntityRegFinalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateEntityRegFinalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateEntityRegFinalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
