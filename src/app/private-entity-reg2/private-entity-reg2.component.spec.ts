import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateEntityReg2Component } from './private-entity-reg2.component';

describe('PrivateEntityReg2Component', () => {
  let component: PrivateEntityReg2Component;
  let fixture: ComponentFixture<PrivateEntityReg2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateEntityReg2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateEntityReg2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
