import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


import {CommonService} from '../services/common-service.service';

@Component({
  selector: 'app-private-entity-reg2',
  templateUrl: './private-entity-reg2.component.html',
  styleUrls: ['./private-entity-reg2.component.css']
})
export class PrivateEntityReg2Component implements OnInit {

  submitReg2: FormGroup;
  entityName: string;
  isOpenValidationAlert: boolean = false;


  noPattern: string = '^(0|[1-9][0-9]*)$';
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";


  validation_messages = {
    entityName: [
      { type: "required", message: "This is a required field." },
      // {
      //   type: "pattern",
      //   message: "Event name should be composed of characters"
      // },
      // {
      //   type: "maxlength",
      //   message: "You can enter a crn of maximum 30 characters."
      // }
    ],
    crNumber: [
      { type: "required", message: "This is a required field." },
      {
        type: "maxlength",
        message: "You can enter CRN of maximum 30 characters."
      }
    ],
    contactNo: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Contact number should be composed of digits."
      },
      {
        type: "maxlength",
        message: "You can enter national Id of maximum 10 characters."
      }
    ],
    emailAddress: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Please enter a valid emailid."
      }
    ],
    address1: [
      { type: "required", message: "This is a required field." }
    ],
    address2: [
      { type: "required", message: "This is a required field." }
    ],

  }

  constructor(private router: Router, private commonService: CommonService, private formBuilder: FormBuilder) {

   }

  ngOnInit() {
    console.log("In private 2::::"+this.commonService.isPrivatePart1Completed);
    if(this.commonService.isPrivatePart1Completed === false){
      this.router.navigate(['/privateEntityReg']);
    }
    this.submitReg2 = this.formBuilder.group({
      entityName: [
        "",
        [
          Validators.required,
        ]
      ],
      crNumber: [
        "",
        [
          Validators.required,
          Validators.maxLength(30)
        ]
      ],
      contactNo: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(10)
        ]
      ],
      emailAddress: [
        "",
        [
          Validators.required,
          Validators.pattern(this.emailPattern)
        ]
      ],
      address1: [
        "",
        [
          Validators.required
        ]
      ],
      address2: [
        "",
        [
          Validators.required
        ]
      ]
    })
    console.log('Reg1 response data in page2::::'+JSON.stringify(this.commonService.userDetails));
    console.log('Reg1 response data in page2::::'+JSON.stringify(this.commonService.userDetails.entityName));
    if(this.commonService.userDetails !== undefined || this.commonService.userDetails !== null){
      this.submitReg2.controls["entityName"].setValue(this.commonService.userDetails.entityName);
      this.submitReg2.controls["crNumber"].setValue(this.commonService.userDetails.crNumber);
      this.submitReg2.controls["contactNo"].setValue(this.commonService.userDetails.contactNo);
      this.submitReg2.controls["emailAddress"].setValue(this.commonService.userDetails.emailAddress);
      this.submitReg2.controls["address1"].setValue(this.commonService.userDetails.address1);
      this.submitReg2.controls["address2"].setValue(this.commonService.userDetails.address2);
   }

  }

  gotoPrevious(){
    this.router.navigate(['/privateEntityReg']);
  }

  saveReg2(){
    if(this.commonService.userDetails !== undefined || this.commonService.userDetails !== null){
      let navigationExtras: NavigationExtras = {
        queryParams: {
            "entityName": this.commonService.userDetails.entityName,
            "crNumber": this.commonService.userDetails.crNumber,
            "contactNo": this.commonService.userDetails.contactNo,
            "emailAddress": this.commonService.userDetails.emailAddress,
            "address1": this.commonService.userDetails.address1,
            "address2": this.commonService.userDetails.address2

        }
    };
    this.commonService.isPrivatePart2Completed = true;
    this.router.navigate(['/privateEntityReg3']);
    // this.router.navigate(["privateEntityReg3"], navigationExtras);
    }
    //this.router.navigate(['/privateEntityReg3']);
  }

  resetForm(){
    if (this.submitReg2.valid) {
      console.log("Form Submitted!");
      this.submitReg2.reset();
    }
  }

}
