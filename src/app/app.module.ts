import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import {AppRoutingModule} from '../app/app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import { PrivateEntityReg1Component } from './private-entity-reg1/private-entity-reg1.component';
import { PrivateEntityReg2Component } from './private-entity-reg2/private-entity-reg2.component';
import { PrivateEntityReg3Component } from './private-entity-reg3/private-entity-reg3.component';
import { PrivateEntityRegFinalComponent } from './private-entity-reg-final/private-entity-reg-final.component';
import { GovernmentEntityReg1Component } from './government-entity-reg1/government-entity-reg1.component';
import { GovernmentEntityReg2Component } from './government-entity-reg2/government-entity-reg2.component';
import { GovernmentEntityReg3Component } from './government-entity-reg3/government-entity-reg3.component';
import { LoginComponent } from './login/login.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { RequestLgIssuanceComponent } from './request-lg-issuance/request-lg-issuance.component';
import { RequestLgIssuanceApprovalComponent } from './request-lg-issuance-approval/request-lg-issuance-approval.component';
import { RequestLgIssuancePaymentComponent } from './request-lg-issuance-payment/request-lg-issuance-payment.component';
import { LandingComponent } from './landing/landing.component';
import { GovEntityDashboardComponent } from './gov-entity-dashboard/gov-entity-dashboard.component';
import { HelpComponent } from './help/help.component';
import { GovPendingActionComponent } from './gov-pending-action/gov-pending-action.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RequestLgIssuanceGovApprovalComponent } from './request-lg-issuance-gov-approval/request-lg-issuance-gov-approval.component';

//Custom pipe
import {SummaryPipe} from './summary.pipe';

//PrimeNg
import {CalendarModule} from 'primeng/calendar';
import {ButtonModule} from 'primeng/button';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {TableModule} from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';



import {MessageService} from 'primeng/api';


/* Tajel  */
import { LoginDialogComponentComponent } from './login-dialog-component/login-dialog-component.component';
import {MatDialogModule, MatButtonModule, MatFormFieldModule, MatRippleModule, MatInputModule} from "@angular/material";
import { OtpDialogComponent } from './otp-dialog/otp-dialog.component';



/*  Tajel   */


@NgModule({
  declarations: [
    AppComponent,
    PrivateEntityReg1Component,
    PrivateEntityReg2Component,
    PrivateEntityReg3Component,
    PrivateEntityRegFinalComponent,
    GovernmentEntityReg1Component,
    GovernmentEntityReg2Component,
    GovernmentEntityReg3Component,
    LoginComponent,
    DashBoardComponent,
    RequestLgIssuanceComponent,
    RequestLgIssuanceApprovalComponent,
    RequestLgIssuancePaymentComponent,
    LandingComponent,
    GovEntityDashboardComponent,
    GovPendingActionComponent,
    HelpComponent,
    ForgotPasswordComponent,
    SummaryPipe,
    RequestLgIssuanceGovApprovalComponent,
    LoginDialogComponentComponent,
    OtpDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ButtonModule,
    CalendarModule,
    MessagesModule,
    MessageModule,
    ConfirmDialogModule,
    TableModule,
    PaginatorModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,


  ],
  providers: [
    MessageService,
    ConfirmationService
  ],


   
  entryComponents: [LoginDialogComponentComponent,OtpDialogComponent],
exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
