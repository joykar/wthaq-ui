import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { Router } from '@angular/router';
import {MatDialogModule, MatDialogConfig, MatDialog} from '@angular/material/dialog';
import { LoginDialogComponentComponent } from '../login-dialog-component/login-dialog-component.component';
import { OtpDialogComponent } from '../otp-dialog/otp-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   
  togglePassView=false;
  submitLogin: FormGroup;
  textPattern = "^[A-Za-z ]*$";

  validation_messages = {
    userId: [
      { type: "required", message: "This is a required field and should be exactly 10 digits only!" },
      {
        type: "pattern",
        message: "National Id should be composed of digits only."
      },
      {
        type: "maxlength",
        message: "National Id must be 10 digits only!."
      },
      {
        type: "minlength",
        message: "National Id must be 10 digits only!."
      }
    ],
    password: [
      { type: "required", message: "This is a required field." }
    ]
  }

  constructor(public dialog: MatDialog, private formBuilder: FormBuilder, private commonService: CommonService, private router: Router) { }

  ngOnInit() {

localStorage.clear();


    this.submitLogin = this.formBuilder.group({
      userId: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.maxLength(10),
          Validators.minLength(10)
        ]
      ],
      password: [
        "",
        [
          Validators.required
        ]
      ]

    })
  }

  onSubmit() {
    console.log("Login data:::" + JSON.stringify(this.submitLogin.value));
    if (this.submitLogin.valid) {
      this.commonService.postLoginDetails(this.submitLogin.value)
        .subscribe(res => {

          console.log("Login Response is::::" + JSON.stringify(res));
 
          if (res.responseCode === 0) {
            localStorage.clear();
            alert("Sorry! We could not log you in. loginid and/or password mismatch. ");
            this.router.navigate(['login']);
            return; 
                   }


          if (res.jwtToken !== undefined  && res.jwtToken !== null) {
             localStorage.setItem('jwtToken', res.jwtToken);
          }
else{
  alert("Sorry! We could not log you in. Received insufficient data.");
  localStorage.clear();
   this.router.navigateByUrl('/login');
return;
}
 
if (res.userObject.loginId !== undefined  && res.userObject.loginId !== null) {
  localStorage.setItem('userId', res.userObject.loginId);
}
else{
  alert("Sorry! We could not log you in. Received insufficient data.");
 localStorage.clear();
this.router.navigateByUrl('/login');
return;
}


/* from below on we can logout using loginid   */


if (res.userObject.roleId !== undefined  && res.userObject.roleId !== null) {
   localStorage.setItem('roleId', res.userObject.roleId);
}
else{
 
  alert("Sorry! We could not log you in. Received insufficient data.");
  localStorage.clear();
//this.router.navigateByUrl('/login');
this.commonService.logout(res.userObject.loginid);
return;
}



if (res.userObject.userName !== undefined  && res.userObject.userName !== null) {
  localStorage.setItem('userName', res.userObject.userName);
}
else{
 alert("Sorry! We could not log you in. Received insufficient data.");
 localStorage.clear();
//this.router.navigateByUrl('/login');
this.commonService.logout(res.userObject.loginid);
return;
}






let OTP=123456;
let mmobile='974.....68';
let uid=res.userObject.loginId;
//alert("uid "+res.userObject.loginId);

let OTPNMobile=this.commonService.otpByLoginId(uid).subscribe(res => {
//alert("res code "+res.responseCode);
if(res.responseCode==1)
{
OTP=res.userObject.OTP;
mmobile=res.userObject.maskedContact;
this.otpDialog(uid,OTP,mmobile);

}
else{
  alert("Sorry! Error occurred while sending OTP. Please try login again.");
  localStorage.clear();
 // this.router.navigateByUrl('/login');
 this.commonService.logout(uid);
}
});
 
 
           
        })
    }

  }

  resetForm(){
    if (this.submitLogin.valid) {
      console.log("Form Submitted!");
      this.submitLogin.reset();
    }
  }
   

otpDialog(loginid,otp,userMobile)
{
  const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass='custom-modalbox';
    dialogConfig.data = {
        
        description:'description',
        loginid:loginid,
        otp:otp,
        userMobile:userMobile
    };
    this.dialog.open(OtpDialogComponent, dialogConfig);
}
viewPassword(){

  this.togglePassView=!this.togglePassView;

}
}
