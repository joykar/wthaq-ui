import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-dialog-component',
  templateUrl: './login-dialog-component.component.html',
  styleUrls: ['./login-dialog-component.component.css']
})
export class LoginDialogComponentComponent implements OnInit {

  form: FormGroup;
    description:string;
    loginid:string;
    crnDetailsByNID:any;
    panelClass:any;

    constructor(private commonService: CommonService,   private router: Router,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<LoginDialogComponentComponent>,
        @Inject(MAT_DIALOG_DATA) data) {

        this.description = data.description;
        this.loginid = data.loginid;
         
    }

    ngOnInit() {

  // put the code from `ngOnInit` here
//for checking login status
 
  
this.commonService.checkSessionExpired(this.loginid)
   .then((data:any)=>{
   
//alert("checkSessionExpired 1234567890: "+data);

if(data==0){
  this.close();
  this.commonService.logout(this.loginid);
  return  ;
}
else{
  this.commonService.populateTableOfCRNByNID(this.loginid)
  .subscribe(res => {

 this.crnDetailsByNID=res.userObject;


  });

  this.dialogRef.backdropClick().subscribe(() => { this.close(); });

    this.form = this.fb.group({
        description: [this.description, []] 
         
    });


}


})
 
     
    }



    redirectByStatus(crn, responseCode,responseMessage){
this.closeOnly();
//alert(responseCode);
      if (responseCode == 100) {
        this.commonService.responseCodeforRegistrationFinal = responseCode;
        this.commonService.alertMsgforRegistrationFinal = responseMessage;
        this.router.navigate(['privateEntityRegFinal']);
      }
      else if (responseCode == 101) {
        this.commonService.responseCodeforRegistrationFinal = responseCode;
        this.commonService.alertMsgforRegistrationFinal = responseMessage;
        this.router.navigate(['privateEntityRegFinal']);
      }
      else if (responseCode == 106) {
        this.commonService.responseCodeforRegistrationFinal = responseCode;
        this.commonService.alertMsgforRegistrationFinal = responseMessage;
        this.router.navigate(['privateEntityRegFinal']);
      }
      else if (responseCode == 104) {
        this.commonService.responseCodeforRegistrationFinal = responseCode;
        this.commonService.alertMsgforRegistrationFinal = responseMessage;
        //go to private entity's dashboard
        this.router.navigate(['dashboard']);
      }
      else if (responseCode == 1 || responseCode == 108) {
        this.router.navigate(['govDashboard']);
      }
      else if (responseCode == 107) {
        this.router.navigate(['govDashboard']);
      }



    }
 

    close() {

      if(confirm("You will be logged out! Do you want to continue?")){
        this.dialogRef.close();
        this.commonService.logout(this.loginid);
        }
         
    }
    warnlogout()
    {
      if(confirm("You will be logged out! Do you want to continue?")){
        this.dialogRef.close();
        this.commonService.logout(this.loginid);
        }
         
      
    }
    closeOnly() {

         this.dialogRef.close();
        
         
    }

    logoutOnNoCRN(){
      this.dialogRef.close();
      this.commonService.logout(this.loginid);
}
}
  
