import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestLgIssuancePaymentComponent } from './request-lg-issuance-payment.component';

describe('RequestLgIssuancePaymentComponent', () => {
  let component: RequestLgIssuancePaymentComponent;
  let fixture: ComponentFixture<RequestLgIssuancePaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestLgIssuancePaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestLgIssuancePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
