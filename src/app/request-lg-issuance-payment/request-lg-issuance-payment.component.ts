import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';

@Component({
  selector: 'app-request-lg-issuance-payment',
  templateUrl: './request-lg-issuance-payment.component.html',
  styleUrls: ['./request-lg-issuance-payment.component.css']
})
export class RequestLgIssuancePaymentComponent implements OnInit {


  submitLGPayment: FormGroup;
  tncPaymentCheckbox: boolean = false;
  isDesableSendButton: boolean = true;


  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService) { }

  ngOnInit() {
    this.submitLGPayment = this.formBuilder.group({
      bankName: [
        ""
      ],
      Account: [
          ""
        ],
        TermsAndCondPayment: [
          ""
        ]
      });
  }

  toggleVisibility(e){
    if(e === true){
      this.isDesableSendButton = false;
    }
    else{
      this.isDesableSendButton = true;
    }
  }

  submitLGIssuancePaymentData(){
    console.log("Submitted payment data::::"+JSON.stringify(this.submitLGPayment.value));
  }

}
