import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovPendingActionComponent } from './gov-pending-action.component';

describe('GovPendingActionComponent', () => {
  let component: GovPendingActionComponent;
  let fixture: ComponentFixture<GovPendingActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovPendingActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovPendingActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
