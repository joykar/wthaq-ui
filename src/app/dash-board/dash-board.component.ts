import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { LGIssuanceModel } from '../models/lgIssuance.model';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {

  userId: string;
  userName: string;
  isOpenPendingList:boolean = true;
  isOpenLGList: boolean = false;
  isPendingClicked: boolean = true;
  isLGClicked: boolean = false;
  pendingList: any[] = [];
  cols: any[];


  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService) { }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    console.log("this.userId::::"+this.userId);
    this.userName = localStorage.getItem('userName');
    console.log("this.userName::::" + this.userName);


    this.cols = [
      { field: 'requestDate', header: 'Request date' },
      { field: 'lGAmount', header: 'LG amount' },
      { field: 'lGType', header: 'LG type' },
      { field: 'beneficiaryName', header: 'Beneficiary name' },
      { field: 'projectName', header: 'Project name' },
      { field: 'bankName', header: 'Bank name' }
    ];

   //To get pending list
   this.commonService.getAllLGRequestBeneficiarySpecificPendingPri(this.userId)
   .subscribe(res => {
     console.log("AllLGRequestBeneficiarySpecific list::::" + JSON.stringify(res));
     this.pendingList = res.userObject;
     console.log("pending list is::::" + JSON.stringify(this.pendingList));

   });
  }

  goToLGListTabPri() {
    // this.isOpenPendingList = true;
    // this.isOpenLGList = false;
    // this.isPendingClicked = true;
    // this.isLGClicked = false;

    this.isOpenPendingList = false;
    this.isOpenLGList = true;
    this.isPendingClicked = false;
    this.isLGClicked = true;

    //To get pending list
    console.log("test");
    this.commonService.getAllLGRequestBeneficiarySpecificPri(this.userId)
      .subscribe(res => {
        console.log("AllLGRequestBeneficiarySpecific list::::" + JSON.stringify(res));
        this.pendingList = res.userObject;
        console.log("pending list is::::" + JSON.stringify(this.pendingList));

      });
  }

  goToPendingReqTabPri() {
    this.isOpenPendingList = true;
    this.isOpenLGList = false;
    this.isPendingClicked = true;
    this.isLGClicked = false;

    //To get pending list
    this.commonService.getAllLGRequestBeneficiarySpecificPendingPri(this.userId)
      .subscribe(res => {
        console.log("AllLGRequestBeneficiarySpecific list::::" + JSON.stringify(res));
        this.pendingList = res.userObject;
        console.log("pending list is::::" + JSON.stringify(this.pendingList));

      });
  }

  RowSelected(pendingData)
  {
    this.commonService.isPrivateLgPending1Completed = true;
    console.log("private entity pending list::::"+JSON.stringify(pendingData));
    console.log("testy122"+pendingData.beneficiary_name);

   this.commonService.lgIssuanceApproval = new LGIssuanceModel();
    if(pendingData.lg_type !== null || pendingData.lg_type !== undefined){
     this.commonService.lgIssuanceApproval.LGType = pendingData.lg_type;
    }
    if(pendingData.beneficiary_name !== null || pendingData.beneficiary_name !== undefined){
      console.log("test beneficiary_name"+pendingData.beneficiary_name);
      this.commonService.lgIssuanceApproval.beneficiary_name = pendingData.beneficiary_name;
     }
    if(pendingData.bank_name !== null || pendingData.bank_name !== undefined){
      console.log("bank_name"+pendingData.bank_name);
     this.commonService.lgIssuanceApproval.bankName = pendingData.bank_name;
    }

    if(pendingData.project_name !== null || pendingData.project_name !== undefined){
      console.log("bank_name"+pendingData.project_name);
     this.commonService.lgIssuanceApproval.project_name = pendingData.project_name;
    }


    if(pendingData.request_date !== null || pendingData.request_date !== undefined){
      console.log("bank_name"+pendingData.request_date);
     this.commonService.lgIssuanceApproval.request_date = pendingData.request_date;
    }




    if(pendingData.request_status_flag !== null || pendingData.request_status_flag !== undefined){
      console.log("bank_name"+pendingData.request_status_flag);
     this.commonService.lgIssuanceApproval.status = pendingData.request_status_flag;
    }





    if(pendingData.lg_start_date !== null || pendingData.lg_start_date !== undefined){
      this.commonService.lgIssuanceApproval.LGStartDate = pendingData.lg_start_date;
     }
     if(pendingData.lg_end_date !== null || pendingData.lg_end_date !== undefined){
      this.commonService.lgIssuanceApproval.LGEndDate = pendingData.lg_end_date;
     }

    if(pendingData.contract_value !== null || pendingData.contract_value !== undefined){
     this.commonService.lgIssuanceApproval.Amount = pendingData.contract_value;
    }

    if(pendingData.currency !== null || pendingData.currency !== undefined){
      this.commonService.lgIssuanceApproval.currency = pendingData.currency;
     }
     if(pendingData.terms_condition_message !== null || pendingData.terms_condition_message !== undefined){
      this.commonService.lgIssuanceApproval.termsNconditionsMessage = pendingData.terms_condition_message;
     }

     if(pendingData.status !== null || pendingData.status !== undefined){
      this.commonService.lgIssuanceApproval.status = pendingData.status;
     }

     if(pendingData.response_code !== null || pendingData.response_code !== undefined){
      this.commonService.lgIssuanceApproval.response_code = pendingData.response_code;
      console.log("............................pendingData.response_code"+pendingData.response_code);

     }


     if(pendingData.terms_condition_message !== null || pendingData.terms_condition_message !== undefined){
      this.commonService.lgIssuanceApproval.terms_condition_message = pendingData.terms_condition_message;
      console.log("........................pendingData.terms_condition_message"+pendingData.terms_condition_message);
     }

     if(pendingData.request_id !== null || pendingData.request_id !== undefined){
      this.commonService.lgIssuanceApproval.request_id = pendingData.request_id;
      console.log("........................pendingData.request_id"+pendingData.request_id);
     }
     if(pendingData.create_by !== null || pendingData.create_by !== undefined){
      this.commonService.lgIssuanceApproval.create_by = pendingData.create_by;

     }

    this.router.navigate(['requestLgIssuanceApproval']);


    //this.lgIssuance.

  }

  logout()
  {
    this.commonService.logout(this.userId);
  }



}
