import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { LGIssuanceModel } from '../models/lgIssuance.model';

@Component({
  selector: 'app-request-lg-issuance-approval',
  templateUrl: './request-lg-issuance-approval.component.html',
  styleUrls: ['./request-lg-issuance-approval.component.css']
})
export class RequestLgIssuanceApprovalComponent implements OnInit {


  submitLGIssuanceApproval: FormGroup;
  isDisableApprove: boolean = true;
  tncCheckbox: boolean = false;
  checkBox: boolean = false;
  lgIssuance: LGIssuanceModel;

  roleId: any;
  isEnableButtons: boolean = false;
  responseCode: any;
  alertMsgforPrivateLGApproval: string;
  request_id: any;
  loginId: any;



  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService) {
    this.roleId = localStorage.getItem('roleId');
    console.log("this.roleId::::" + this.roleId);
    this.loginId = localStorage.getItem('loginId');
    console.log("this.loginId::::" + this.loginId);

    console.log("this.commonService.lgIssuanceApproval*****"+JSON.stringify(this.commonService.lgIssuanceApproval));

    if(this.commonService.lgIssuanceApproval !== undefined || this.commonService.lgIssuanceApproval !== null){
      if(this.commonService.lgIssuanceApproval.request_id !== undefined || this.commonService.lgIssuanceApproval.request_id !== null){
        this.request_id = this.commonService.lgIssuanceApproval.request_id;
      }
      console.log("this.commonService.lgIssuanceApproval.create_by in private::::"+this.commonService.lgIssuanceApproval.create_by);
    }

    if(this.roleId !== null || this.roleId !== undefined){
      //if(this.roleId == 1){
        if(this.loginId == this.commonService.lgIssuanceApproval.create_by){
        console.log("this.roleId in this.commonService.lgIssuanceApproval:::::"+this.roleId);
        //PENDING
        //if(this.commonService.lgIssuanceApproval.status != null || this.commonService.lgIssuanceApproval.status != undefined){
         if(this.commonService.lgIssuanceApproval.response_code == '120'){
          this.isEnableButtons = false;
          this.alertMsgforPrivateLGApproval = "Your case request has been sent to bank. You will be notified once approved.";
         }
         // NS-CHANGE
         else if(this.commonService.lgIssuanceApproval.response_code == '121'){
          this.isEnableButtons = true;
          this.alertMsgforPrivateLGApproval = "Your case request has been modified by the Beneficiary. Please accept for further processing.";
         }
         //NS-PENDING
         else if(this.commonService.lgIssuanceApproval.response_code == '124'){
          this.isEnableButtons = false;
          this.alertMsgforPrivateLGApproval = "Your case request has been sent to Beneficiary. Please contact them for approving the Request.";
         }
         //NS-ACCEPT
         else if(this.commonService.lgIssuanceApproval.response_code == '122'){
          this.isEnableButtons = false;
          this.alertMsgforPrivateLGApproval = "Accepted by private entity.";
         }
         //CANCELLED
         else if(this.commonService.lgIssuanceApproval.response_code == '123'){
          this.isEnableButtons = false;
          this.alertMsgforPrivateLGApproval = "Cancelled by private entity";
         }


       //}

      }
    }

  }

  ngOnInit() {

    this.submitLGIssuanceApproval = this.formBuilder.group({
      LGType: [
        ""
      ],
      // bankName: [
      //   ""
      // ],
      Amount: [
        ""
      ],

      currency: [
        ""
      ],
      LGStartDate: [
        ""
      ],
      LGEndDate: [
        ""
      ],
      TermsAndCond: [
        ""
      ],
      TermsAndCondContent: [
        ""
      ]

    })

    //console.log("this.commonService.lgIssuanceApproval::::"+JSON.stringify(this.commonService.lgIssuanceApproval));



    if(this.commonService.lgIssuanceApproval.LGType != null || this.commonService.lgIssuanceApproval.LGType != undefined){
      this.submitLGIssuanceApproval.controls["LGType"].setValue(this.commonService.lgIssuanceApproval.LGType);
   }

   if(this.commonService.lgIssuanceApproval.bankName != null || this.commonService.lgIssuanceApproval.bankName != undefined){
    this.submitLGIssuanceApproval.controls["bankName"].setValue(this.commonService.lgIssuanceApproval.bankName);
   }
   if(this.commonService.lgIssuanceApproval.LGStartDate != null || this.commonService.lgIssuanceApproval.LGStartDate != undefined){
    this.submitLGIssuanceApproval.controls["LGStartDate"].setValue(this.commonService.lgIssuanceApproval.LGStartDate);
   }
   if(this.commonService.lgIssuanceApproval.LGEndDate != null || this.commonService.lgIssuanceApproval.LGEndDate != undefined){
    this.submitLGIssuanceApproval.controls["LGEndDate"].setValue(this.commonService.lgIssuanceApproval.LGEndDate);
   }
   if(this.commonService.lgIssuanceApproval.Amount != null || this.commonService.lgIssuanceApproval.Amount != undefined){
    this.submitLGIssuanceApproval.controls["Amount"].setValue(this.commonService.lgIssuanceApproval.Amount);
   }
   if(this.commonService.lgIssuanceApproval.currency != null || this.commonService.lgIssuanceApproval.currency != undefined){
    this.submitLGIssuanceApproval.controls["currency"].setValue(this.commonService.lgIssuanceApproval.currency);
   }

   if(this.commonService.lgIssuanceApproval.termsNconditionsMessage != null || this.commonService.lgIssuanceApproval.termsNconditionsMessage != undefined){
    this.submitLGIssuanceApproval.controls["TermsAndCondContent"].setValue(this.commonService.lgIssuanceApproval.termsNconditionsMessage);
   }



  }


  rejectLGIssuanceApprovalData(){
    console.log("this.request_id::::"+this.request_id);
    this.commonService.CancellingLgRequestFromSupplier(this.request_id)
   .subscribe(res => {
     console.log("rejectLGIssuanceApprovalData response::::" + JSON.stringify(res));
   });

  }

  acceptLGIssuanceApprovalData(){
    console.log("this.request_id::::"+this.request_id);
    this.commonService.AcceptingLgRequestFromSupplier(this.request_id)
   .subscribe(res => {
     console.log("AcceptingLgRequestFromSupplier response::::" + JSON.stringify(res));
   });

  }

}
