import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestLgIssuanceApprovalComponent } from './request-lg-issuance-approval.component';

describe('RequestLgIssuanceApprovalComponent', () => {
  let component: RequestLgIssuanceApprovalComponent;
  let fixture: ComponentFixture<RequestLgIssuanceApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestLgIssuanceApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestLgIssuanceApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
