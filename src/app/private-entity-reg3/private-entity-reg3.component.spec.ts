import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateEntityReg3Component } from './private-entity-reg3.component';

describe('PrivateEntityReg3Component', () => {
  let component: PrivateEntityReg3Component;
  let fixture: ComponentFixture<PrivateEntityReg3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateEntityReg3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateEntityReg3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
