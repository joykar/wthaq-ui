import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDetailsModel } from '../models/userDetails.model';
import { CommonService } from '../services/common-service.service';

@Component({
  selector: 'app-private-entity-reg3',
  templateUrl: './private-entity-reg3.component.html',
  styleUrls: ['./private-entity-reg3.component.css']
})
export class PrivateEntityReg3Component implements OnInit {


  rniFile: any;
  lalFile: any;
  crFile: any;
  laglFile: any;
  rniFileName: any;
  lalFileName: any;
  crFileName: any;
  laglFileName: any;
  rniFileExtension: any;
  lalFileExtension: any;
  crFileExtension: any;
  laglFileExtension: any;
  notSelected: string ="Not selected";

  isSelectedrniFile: boolean = false;
  isSelectedlalFile: boolean = false;
  isSelectedcrFile: boolean = false;
  isSelectedlaglFile: boolean = false;
  isOpenValidationAlert: boolean = false;
  isOpenModal: boolean = false;


  userDetails: UserDetailsModel;

  submitReg3: FormGroup;

  noPattern: string = '^(0|[1-9][0-9]*)$';
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  textPattern = "^[A-Za-z ]*$";
  namePattern = "^[A-Za-z]+$";


  allowedExtensions = ['jpg', 'gif', 'pdf', 'png'];

  validation_messages = {
    name: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Name should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ],
    userName: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Name should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ],
    email: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Please enter a valid emailid."
      }
    ],
    nationalId: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "National Id should be composed of digits."
      },
      {
        type: "maxlength",
        message: "You can enter national Id of maximum 9 digits."
      }
    ],
    userId: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "User Id should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ],
    mobileNo: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Contact number should be composed of digits."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 10."
      },
      {
        type: "minlength",
        message: "Minimum length should be 10."
      }
    ],
    rniFile: [
      { type: "required", message: "This is a required field." }
    ],
    crFile: [
      { type: "required", message: "This is a required field." }
    ],
    laglFile: [
      { type: "required", message: "This is a required field." }
    ],
    lalFile: [
      { type: "required", message: "This is a required field." }
    ],


  }


  constructor(private router: Router, private commonService: CommonService, private formBuilder: FormBuilder) {
    console.log("I'm in third page" + JSON.stringify(this.commonService.userDetails));


  }

  ngOnInit() {
    console.log("In private 3::::"+this.commonService.isPrivatePart2Completed);
    if(this.commonService.isPrivatePart2Completed === false && this.commonService.isPrivatePart1Completed === true){
      this.router.navigate(['/privateEntityReg2']);
    }
    if(this.commonService.isPrivatePart2Completed === false && this.commonService.isPrivatePart1Completed === false){
      this.router.navigate(['/privateEntityReg']);
    }

    this.submitReg3 = this.formBuilder.group({
      name: [
        "",
        [
          Validators.required,
          Validators.pattern(this.namePattern),
          Validators.maxLength(20)
        ]
      ],
      userName: [
        "",
        [
          Validators.required,
          Validators.pattern(this.textPattern),
          Validators.maxLength(20)
        ]
      ],
      email: [
        "",
        [
          Validators.required,
          Validators.pattern(this.emailPattern),
        ]
      ],
      userId: [
        "",
        [
          Validators.required,
          Validators.pattern(this.textPattern),
          Validators.maxLength(20)
        ]
      ],
      mobileNo: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(10),
          Validators.minLength(10)
        ]
      ],
      nationalId: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(9)
        ]
      ],
      rniFile: [
        "",
        [
          Validators.required
        ]
      ],
      crFile: [
        "",
        [
          Validators.required
        ]
      ],
      laglFile: [
        "",
        [
          Validators.required
        ]
      ],
      lalFile: [
        "",
        [
          Validators.required
        ]
      ]
    });


  }

  onBlur(userId){
    console.log("userId:::::"+userId);
    this.checkValidLoginId(userId);
  }

  checkValidLoginId(userId){
    this.commonService.checkValidLoginId(userId)
      .subscribe(res => {
        console.log("Response for userId is::::"+JSON.stringify(res));
        if(res.responseCode === 0){
          alert(res.responseMessage);
          this.submitReg3.controls["userId"].setValue(null);
        }

      })
  }

  fileProgress(event: any, fileCat: String) {

    if (fileCat === 'rni') {

      let fileData = <File>event.target.files[0];

      if(fileData === undefined){
        this.rniFile = null;
        this.isSelectedrniFile = false;
      }

      if (fileData.size > 500000) {
        alert("Please upload < 500KB");
        fileData = undefined;
        this.rniFile = null;
        this.isSelectedrniFile = false;

      }
      let fname = fileData.name.split('.');
      this.rniFileName = fname[0];
      this.rniFileExtension = fname[1];
      console.log("rni fname::::"+fname[0]);
      if (!this.allowedExtensions.some((item) => item == (fname[fname.length - 1]).toLowerCase())) {
        alert(fname[fname.length - 1] + " file format is not allowed. Please upload recommended file types.");
        //  return this.isValidationOK=false;
      }
      else {
        this.rniFile = fileData;
        this.isSelectedrniFile = true;
        this.commonService.userDetails.rniFile = fileData;
      }
    }
    else if (fileCat === 'lal') {

      let fileData = <File>event.target.files[0];

      if(fileData === undefined){
        this.lalFile = null;
        this.isSelectedlalFile = false;
      }

      if (fileData.size > 500000) {
        alert("Please upload < 500KB");

        fileData = undefined;
        this.lalFile = null;
        this.isSelectedlalFile = false;

      }
      let fname = fileData.name.split('.');
      this.lalFileName = fname[0];
      this.lalFileExtension = fname[1];
      console.log("lal fname::::"+fname);
      if (!this.allowedExtensions.some((item) => item == (fname[fname.length - 1]).toLowerCase())) {
        alert(fname[fname.length - 1] + " file format is not allowed. Please upload recommended file types.");
        //  return this.isValidationOK=false;
      }
      else {
        this.lalFile = fileData;
        this.isSelectedlalFile = true;
        this.commonService.userDetails.lalFile = fileData;
      }
    }
    else if (fileCat === 'lagl') {
      let fileData = <File>event.target.files[0];


      if(fileData === undefined){
        this.laglFile = null;
        this.isSelectedlaglFile = false;
      }

      if (fileData.size > 500000) {
        alert("Please upload < 500KB");
        fileData = undefined;
        this.laglFile = null;
        this.isSelectedlaglFile = false;

      }
      let fname = fileData.name.split('.');
      this.laglFileName = fname[0];
      this.laglFileExtension = fname[1];
      console.log("lagl fname::::"+fname);
      if (!this.allowedExtensions.some((item) => item == (fname[fname.length - 1]).toLowerCase())) {
        alert(fname[fname.length - 1] + " file format is not allowed. Please upload recommended file types.");
        //  return this.isValidationOK=false;
      }
      else {
        this.laglFile = fileData;
        this.isSelectedlaglFile = true;
        this.commonService.userDetails.laglFile = fileData;
      }

    }
    else if (fileCat === 'cr') {
      let fileData = <File>event.target.files[0];

      if(fileData === undefined){
        this.crFile = null;
        this.isSelectedcrFile = false;
      }

      if (fileData.size > 500000) {
        alert("Please upload < 500KB");
        fileData = undefined;
        this.crFile = null;
        this.isSelectedcrFile = false;

      }
      let fname = fileData.name.split('.');
      this.crFileName = fname[0];
      this.crFileExtension = fname[1];
      console.log("cr fname::::"+fname);
      if (!this.allowedExtensions.some((item) => item == (fname[fname.length - 1]).toLowerCase())) {
        alert(fname[fname.length - 1] + " file format is not allowed. Please upload recommended file types.");
        //  return this.isValidationOK=false;
      }
      else {
        this.crFile = fileData;
        this.isSelectedcrFile = true;
        this.commonService.userDetails.crFile = fileData;
      }
  }

  // if((this.rniFileName === this.lalFileName) || (this.rniFileName === this.laglFileExtension) || (this.rniFileName === this.crFileName)) {
  //   alert("Attachment file should not be same");
  // }
}

// fileProgress(event: any, fileCat: String) {


//   if (fileCat === 'rni') {
//     let fileData = <File>event.target.files[0];
//       this.commonService.userDetails.rniFile = fileData;
//       this.rniFile = fileData;

//       this.isSelectedrniFile = true;

//   }
//   else if (fileCat === 'cr') {
//     let fileData = <File>event.target.files[0];
//     this.isSelectedcrFile = true;
//     this.commonService.userDetails.crFile = fileData;
//     this.crFile = fileData;


//   }
//   else if (fileCat === 'lal') {
//     let fileData = <File>event.target.files[0];
//     this.isSelectedlalFile = true;
//     this.commonService.userDetails.lalFile = fileData;
//     this.lalFile = fileData;

//   }
//   else if (fileCat === 'lagl') {
//     let fileData = <File>event.target.files[0];
//     this.isSelectedlaglFile = true;
//     this.commonService.userDetails.laglFile = fileData;
//     this.laglFile = fileData;
//   }

// }




  gotoPrevious() {
    this.router.navigate(['/privateEntityReg2']);
  }
  saveReg3() {
    console.log("Reg 3 data:::::" + JSON.stringify(this.submitReg3.value));
    if (this.submitReg3.valid) {
      // this.commonService.userDetails.rniFile = this.submitReg3.value.rniFile;
      // this.commonService.userDetails.crFile = this.submitReg3.value.crFile;
      // this.commonService.userDetails.lalFile = this.submitReg3.value.lalFile;
      // this.commonService.userDetails.laglFile = this.laglFile;

      this.commonService.userDetails.name = this.submitReg3.value.name;
       this.commonService.userDetails.userName = this.submitReg3.value.userName;
       this.commonService.userDetails.email = this.submitReg3.value.email;
       this.commonService.userDetails.userId = this.submitReg3.value.userId;
       this.commonService.userDetails.mobileNo = this.submitReg3.value.mobileNo;
       this.commonService.userDetails.nationalId = this.submitReg3.value.nationalId;


      console.log("this.commonService.userDetails::::"+JSON.stringify(this.commonService.userDetails));

      this.commonService.postRegistrationDetails(this.commonService.userDetails)
      .subscribe(res => {
        console.log("Response is::::"+JSON.stringify(res));
        //this.commonService.userDetails = res;
        if(res.responseCode == 1){
          this.commonService.responseCodeforRegistrationFinal = res.responseCode;
          this.commonService.alertMsgforRegistrationFinal = res.responseMessage;
          this.commonService.isPrivatePart3Completed = true;
          //this.isOpenModal = true;
          //alert("Your documents and details are pending for verification. You will be notified through the communication details you provided.")
          this.router.navigate(['/privateEntityRegFinal']);
        }

      })

    }
    else{
      this.isOpenValidationAlert = true;
    }

  }
  resetForm(){
    if (this.submitReg3.valid) {
      console.log("Form Submitted!");
      this.submitReg3.reset();
    }
  }
  closeModal(){
    this.isOpenModal = false;
  }

  closeAlert(){
    this.isOpenValidationAlert = false;
  }

}
