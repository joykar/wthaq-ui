import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestLgIssuanceGovApprovalComponent } from './request-lg-issuance-gov-approval.component';

describe('RequestLgIssuanceGovApprovalComponent', () => {
  let component: RequestLgIssuanceGovApprovalComponent;
  let fixture: ComponentFixture<RequestLgIssuanceGovApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestLgIssuanceGovApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestLgIssuanceGovApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
