import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';
import { LGIssuanceModel } from '../models/lgIssuance.model';

@Component({
  selector: 'app-request-lg-issuance-gov-approval',
  templateUrl: './request-lg-issuance-gov-approval.component.html',
  styleUrls: ['./request-lg-issuance-gov-approval.component.css']
})
export class RequestLgIssuanceGovApprovalComponent implements OnInit {

  submitLGIssuanceApproval: FormGroup;
  isDisableApprove: boolean = true;
  tncCheckbox: boolean = false;
  lgIssuance: LGIssuanceModel;
  isEditTnC: boolean = false;
  TermsAndCondContent: string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
  defaultTermsAndCondContent: string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
  roleId: any;
  loginId: any;
  isEnableButtons: boolean = false;
  isDisablesendBackButton: boolean = true;
  isDisabledApprovedButton: boolean = false;
  isAllowToApprove: boolean = false;
  alertMsgforGovLGApproval: string;
  request_id: any;
  checkBox: boolean = false;
  // isEnablePrivateLGApprovalbuttons: string;
  // isEnablePrivateApproval: boolean = false;


  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService) {
    this.roleId = localStorage.getItem('roleId');
    console.log("this.roleId::::" + this.roleId);
    this.loginId = localStorage.getItem('loginId');
    console.log("this.loginId::::" + this.loginId);

    console.log("this.commonService.lgIssuanceApproval*****"+JSON.stringify(this.commonService.lgIssuanceApproval));

    if(this.commonService.lgIssuanceApproval !== undefined || this.commonService.lgIssuanceApproval !== null){
      if(this.commonService.lgIssuanceApproval.request_id !== undefined || this.commonService.lgIssuanceApproval.request_id !== null){
        this.request_id = this.commonService.lgIssuanceApproval.request_id;
      }

      console.log("this.commonService.lgIssuanceApproval.create_by::::"+this.commonService.lgIssuanceApproval.create_by);
    }

    if(this.roleId !== null || this.roleId !== undefined){
      if(this.loginId != this.commonService.lgIssuanceApproval.create_by){
      //if(this.roleId == 2 ){

        //PENDING
        //if(this.commonService.lgIssuanceApproval.status != null || this.commonService.lgIssuanceApproval.status != undefined){
         if(this.commonService.lgIssuanceApproval.response_code == '120'){
          this.isEnableButtons = false;
          this.alertMsgforGovLGApproval = "Your case request has been sent to bank. You will be notified once approved.";
         }
         // NS-CHANGE
         else if(this.commonService.lgIssuanceApproval.response_code == '121'){
          this.isEnableButtons = true;
          this.alertMsgforGovLGApproval = "Your case request has been modified by the Beneficiary. Please accept for further processing.";
         }
         //NS-PENDING
         else if(this.commonService.lgIssuanceApproval.response_code == '124'){
           console.log("this.isEnableButtons for response_code == '124'::::"+this.isEnableButtons);
          this.isEnableButtons = true;
          this.alertMsgforGovLGApproval = "Your case request has been sent to Beneficiary. Please contact them for approving the Request.";
         }
         //NS-ACCEPT
         else if(this.commonService.lgIssuanceApproval.response_code == '122'){
          this.isEnableButtons = false;
          this.alertMsgforGovLGApproval = "Request Accepted By Supplier";
         }
         //CANCELLED
         else if(this.commonService.lgIssuanceApproval.response_code == '123'){
          this.isEnableButtons = false;
          this.alertMsgforGovLGApproval = "Cancelled by private entity";
         }


       //}

      }
    }



  }

  ngOnInit() {


    // if(this.roleId !== null || this.roleId !== undefined){
    //   //if roalId is 2 then it means gov
    //   if(this.roleId == 2){
    //     this.isEnableButtons = true;
    //     this.commonService.alertMsgforLGApproval = "Your new case request is waiting for approval"

    //   }

    //   // else if( this.roleId == 1 && this.isEnablePrivateLGApprovalbuttons === 'true'){
    //   //   this.isEnablePrivateApproval = true;
    //   //   this.commonService.alertMsgforLGApproval = "Your new case is pending for your acceptance.";

    //   // }
    //   else if( this.roleId == 1){
    //     this.isEnableButtons = false;
    //     this.isEnablePrivateApproval = false;
    //     this.commonService.alertMsgforLGApproval = "Your new case request is sent for approval";
    //   }

    // }


    this.submitLGIssuanceApproval = this.formBuilder.group({
      LGType: [
        ""
      ],
      bankName: [
        ""
      ],
      Amount: [
        ""
      ],

      currency: [
        ""
      ],
      LGStartDate: [
        ""
      ],
      LGEndDate: [
        ""
      ],
      TermsAndCond: [
        ""
      ],
      TermsAndCondContent: [
        this.TermsAndCondContent
      ]

    })

    console.log("this.commonService.lgIssuanceApproval::::"+JSON.stringify(this.commonService.lgIssuanceApproval));

    if(this.commonService.lgIssuanceApproval.LGType != null || this.commonService.lgIssuanceApproval.LGType != undefined){
      this.submitLGIssuanceApproval.controls["LGType"].setValue(this.commonService.lgIssuanceApproval.LGType);
   }

   if(this.commonService.lgIssuanceApproval.bankName != null || this.commonService.lgIssuanceApproval.bankName != undefined){
    this.submitLGIssuanceApproval.controls["bankName"].setValue(this.commonService.lgIssuanceApproval.bankName);
   }
   if(this.commonService.lgIssuanceApproval.LGStartDate != null || this.commonService.lgIssuanceApproval.LGStartDate != undefined){
    this.submitLGIssuanceApproval.controls["LGStartDate"].setValue(this.commonService.lgIssuanceApproval.LGStartDate);
   }
   if(this.commonService.lgIssuanceApproval.LGEndDate != null || this.commonService.lgIssuanceApproval.LGEndDate != undefined){
    this.submitLGIssuanceApproval.controls["LGEndDate"].setValue(this.commonService.lgIssuanceApproval.LGEndDate);
   }
   if(this.commonService.lgIssuanceApproval.Amount != null || this.commonService.lgIssuanceApproval.Amount != undefined){
    this.submitLGIssuanceApproval.controls["Amount"].setValue(this.commonService.lgIssuanceApproval.Amount);
   }
   if(this.commonService.lgIssuanceApproval.currency != null || this.commonService.lgIssuanceApproval.currency != undefined){
    this.submitLGIssuanceApproval.controls["currency"].setValue(this.commonService.lgIssuanceApproval.currency);
   }

   if(this.commonService.lgIssuanceApproval.termsNconditionsMessage != null || this.commonService.lgIssuanceApproval.termsNconditionsMessage != undefined){
    this.submitLGIssuanceApproval.controls["TermsAndCondContent"].setValue(this.commonService.lgIssuanceApproval.termsNconditionsMessage);
   }

  }

  toggleVisibility(e) {

    console.log("this.theCheckbox::::" + e);
    if(e === false){
      this.isDisableApprove = true;
      this.isAllowToApprove = false;
    }
    else{
      this.isDisableApprove = false;
      this.isAllowToApprove = true;
    }
  }

  editLGIssuanceApprovalData(){
    this.isEditTnC = true;
    this.isDisablesendBackButton = false;
    this.isDisabledApprovedButton = true;

    console.log("I'm in editLGIssuanceApprovalData::::"+this.isEditTnC);
  }

  sendBackLGIssuanceApprovalData(){

    if(this.isEditTnC === true){
        this.commonService.postChangeedTnCOfLgRequestBeneficiary(this.commonService.lgIssuanceApproval.requestId, this.submitLGIssuanceApproval.value.TermsAndCondContent)
        .subscribe(res => {
          console.log("LG approval Response is::::"+JSON.stringify(res));
          if(res.responseCode === 121){
            alert("responce code"+res.responseCode);
            this.alertMsgforGovLGApproval = res.responseMessage;
            localStorage.setItem('responseCode', res.responseCode);

          }
        });
      }
  }

  updateTnC(){
    this.isEditTnC = false;
  }

  submitLGIssuanceApprovalData(){
    if(this.isAllowToApprove === true){
      this.commonService.postTnCOfLgRequestBeneficiary(this.commonService.lgIssuanceApproval.requestId)
        .subscribe(res => {
          console.log("LG approval Response is::::"+JSON.stringify(res));
          if(res.responseCode === 120){
            this.alertMsgforGovLGApproval = res.responseMessage;
            this.commonService.responseCode = res.responseCode;
            localStorage.setItem('responseCode', res.responseCode);
          }
        });
    }


  }

  resetData(){
    this.isDisablesendBackButton = true;
    this.isDisabledApprovedButton = false;
    this.submitLGIssuanceApproval.controls["TermsAndCondContent"].setValue(this.defaultTermsAndCondContent);
  }


  rejectLGIssuanceApprovalData(){

  }

  acceptLGIssuanceApprovalData(){

  }


}
