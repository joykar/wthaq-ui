import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialogConfig, MatDialog} from "@angular/material";
import { CommonService } from '../services/common-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LoginDialogComponentComponent } from '../login-dialog-component/login-dialog-component.component';
@Component({
  selector: 'app-otp-dialog',
  templateUrl: './otp-dialog.component.html',
  styleUrls: ['./otp-dialog.component.css']
})
export class OtpDialogComponent implements OnInit {
  loginid:string;
  otp: string;
  //userOTP:string;
  userMobile:string;
  submitOTPLogin: FormGroup;
 // userOTP:   FormControl;
 panelClass; any;
 typedOTP:string;


 validation_messages = {
  userOTP: [
    { type: "required", message: "This is a required field." },
    {
      type: "maxlength",
      message: "Your OTP has only four digits."
    } 
  ]
}


  constructor(public dialog: MatDialog,private formBuilder: FormBuilder,private commonService: CommonService,   private router: Router,
   
    private dialogRef: MatDialogRef<OtpDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data, route:ActivatedRoute) {
      this.loginid = data.loginid;
      this.otp=data.otp;
      this.userMobile = data.userMobile;
      
 
     }

  ngOnInit() 
  {


    
    
     
  // put the code from `ngOnInit` here
//for checking login status
 
//alert("otp-dialog ngOnInit "+this.loginid);
//alert(" call commomn service " +this.commonService.checkSessionExpired(this.loginid));
 
this.commonService.checkSessionExpired(this.loginid)
   .then((data:any)=>{
   
//alert("checkSessionExpired 1234567890: "+data);
if(data==0){
  this.close();
  this.commonService.logout(this.loginid);
  return  ;
}
else{

  this.submitOTPLogin = this.formBuilder.group({
    userOTP: [
      "",
      [
        Validators.required,
        Validators.maxLength(4)
      ]
    ] 

  });

  this.typedOTP=this.otp;



}


})


/* if(this.commonService.checkSessionExpired(this.loginid)==0){
 alert("otp-dialog ngOnInit checkSessionExpired");
  this.close();
 this.commonService.logout(this.loginid);
 return  ;
} 
*/




  


  }


  onOTPSubmit() {
    
//alert(this.otp+" -  "+this.submitOTPLogin.get('userOTP').value+"  -  "+this.typedOTP);

if(this.otp==this.typedOTP /*this.submitOTPLogin.get('userOTP').value*/){
this.close();
  this.openDialog(this.loginid);
}
else{

   alert("Please type the correct OTP");
}


  }

  onOTPLogout(){

if(confirm("You will be logged out! Do you want to continue?")){
this.close();
this.commonService.logout(this.loginid);
}
else{
}

  }

  openDialog(loginid) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass='custom-modalbox';
    dialogConfig.data = {
        id: 1,
        title: 'Angular For Beginners',
        description:'description',
        loginid:loginid
    };

    this.dialog.open(LoginDialogComponentComponent, dialogConfig);
}

close() {
  this.dialogRef.close();
  this.dialogRef.afterClosed().subscribe((result) => {
    if (result === 'closed') {
         this.router.navigate(['login']);
    }
  });
}

onOTPResend(){
//if(this.commonService.checkSessionExpired(this.loginid)==0){return;}
  

this.commonService.otpByLoginId(this.loginid)
     .subscribe(res => {
if(res.responseCode==0){
alert(res.responseMessage);
}
else { 
  this.userMobile=res.maskedContact;
  this.otp=res.userObject.OTP;  
  this.typedOTP=res.userObject.OTP;/*  ONLY WHEN BOTH OTP ARE SAME (FOR TEST ONLY) */

alert(res.responseMessage);
}  
});
}


}
