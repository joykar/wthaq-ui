import { Component, OnInit } from '@angular/core'; import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';

@Component({
  selector: 'app-government-entity-reg3',
  templateUrl: './government-entity-reg3.component.html',
  styleUrls: ['./government-entity-reg3.component.css']
})
export class GovernmentEntityReg3Component implements OnInit {


  submitGov3: FormGroup;
  isOpenValidationAlert: boolean = false;
  textPattern = "^[A-Za-z ]*$";

  validation_messages = {
    userId: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "User Id should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ]
  }

  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService) {
    console.log("in gov page 3:::" + JSON.stringify(this.commonService.govEntityDEtails));
  }

  ngOnInit() {
    console.log("In government 3::::"+this.commonService.isGovPart2Completed);
    if(this.commonService.isGovPart2Completed === false && this.commonService.isGovPart1Completed === true){
      this.router.navigate(['/governmentEntityReg2']);
    }
    if(this.commonService.isGovPart2Completed === false && this.commonService.isGovPart1Completed === false){
      this.router.navigate(['/governmentEntityReg']);
    }

    this.submitGov3 = this.formBuilder.group({
      userId: [
        "",
        [
          Validators.required,
          Validators.pattern(this.textPattern),
          Validators.maxLength(20)
        ]
      ],
    });
  }

  onBlur(userId){
    console.log("userId:::::"+userId);
    this.checkValidLoginId(userId);
  }

  checkValidLoginId(userId){
    this.commonService.checkValidLoginId(userId)
      .subscribe(res => {
        console.log("Response for userId is::::"+JSON.stringify(res));
        if(res.responseCode === 0){
          alert(res.responseMessage);
          this.submitGov3.controls["userId"].setValue(null);
        }

      })
  }

  gotoPrevious() {
    this.router.navigate(['/governmentEntityReg2']);
  }

  saveReg3() {
    console.log("Reg 3 data:::::" + JSON.stringify(this.submitGov3.value));
    if (this.submitGov3.valid) {
      this.commonService.govEntityDEtails.userId = this.submitGov3.controls['userId'].value;
      console.log("Final reg details::::" + JSON.stringify(this.commonService.govEntityDEtails));

      this.commonService.postGovEntityDetails(this.commonService.govEntityDEtails)
        .subscribe(res => {
          console.log("Gov Reg final page Response is::::" + JSON.stringify(res));
          if (res.responseCode == 1) {
            this.router.navigate(['login']);
          }
        });
      //this.router.navigate(['governmentEntityReg3']);
      this.commonService.isGovPart3Completed = true;
    }
    else {
      this.isOpenValidationAlert = true;
    }
  }

  closeAlert() {
    this.isOpenValidationAlert = false;
  }

}
