import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovernmentEntityReg3Component } from './government-entity-reg3.component';

describe('GovernmentEntityReg3Component', () => {
  let component: GovernmentEntityReg3Component;
  let fixture: ComponentFixture<GovernmentEntityReg3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovernmentEntityReg3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernmentEntityReg3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
