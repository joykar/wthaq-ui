import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../services/common-service.service';

@Component({
  selector: 'app-government-entity-reg2',
  templateUrl: './government-entity-reg2.component.html',
  styleUrls: ['./government-entity-reg2.component.css']
})
export class GovernmentEntityReg2Component implements OnInit {

  submitGov2: FormGroup;
  rniFile: any;
  lalFile: any;
  laglFile: any;
  lalFileName: any;
  rniFileName: any;
  laglFileName: any;
  notSelected: string = "Not selected";

  isSelectedrniFile: boolean = false;
  isSelectedlalFile: boolean = false;
  isSelectedlaglFile: boolean = false;
  isOpenValidationAlert: boolean = false;
  textPattern = "^[A-Za-z ]*$";
  addressPattern = "^[A-Za-z]+$";

  allowedExtensions = ['jpg', 'gif', 'pdf', 'png'];

  validation_messages = {
    address1: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Address 1 should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }

    ],
    address2: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Address 2 should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ],
    country: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "Country should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ],
    city: [
      { type: "required", message: "This is a required field." },
      {
        type: "pattern",
        message: "City should be composed of characters."
      },
      {
        type: "maxlength",
        message: "Maximum length should be 20."
      }
    ],
    lalFile: [
      { type: "required", message: "This is a required field." }
    ],
    rniFile: [
      { type: "required", message: "This is a required field." }
    ],
    laglFile: [
      { type: "required", message: "This is a required field." }
    ]

  }


  constructor(private router: Router, private formBuilder: FormBuilder, private commonService: CommonService) {
    console.log("in gov page 2:::" + JSON.stringify(this.commonService.govEntityDEtails));

  }

  ngOnInit() {

    console.log("In government 2::::"+this.commonService.isGovPart1Completed);
    if(this.commonService.isGovPart1Completed === false){
      this.router.navigate(['/governmentEntityReg']);
    }

    this.submitGov2 = this.formBuilder.group({
      address1: [
        "",
        [
          Validators.required,
          Validators.pattern(this.addressPattern),
          Validators.maxLength(20)
        ]
      ],
      address2: [
        "",
        [
          Validators.required,
          Validators.pattern(this.addressPattern),
          Validators.maxLength(20)
        ]
      ],
      country: [
        "",
        [
          Validators.required,
          Validators.pattern(this.addressPattern),
          Validators.maxLength(20)
        ]
      ],
      city: [
        "",
        [
          Validators.required,
          Validators.pattern(this.addressPattern),
          Validators.maxLength(20)
        ]
      ],
      lalFile: [
        "",
        [
          Validators.required,
        ]
      ],
      rniFile: [
        "",
        [
          Validators.required
        ]
      ],
      laglFile: [
        "",
        [
          Validators.required
        ]
      ]

    })


    if(this.commonService.govEntityDEtails !== undefined || this.commonService.govEntityDEtails !== null){

      if(this.commonService.govEntityDEtails.address1 !== undefined || this.commonService.govEntityDEtails.address1 !== null){
        this.submitGov2.controls["address1"].setValue(this.commonService.govEntityDEtails.address1);
      }
      if(this.commonService.govEntityDEtails.address2 !== undefined || this.commonService.govEntityDEtails.address2 !== null){
        this.submitGov2.controls["address2"].setValue(this.commonService.govEntityDEtails.address2);
      }
      if(this.commonService.govEntityDEtails.country !== undefined || this.commonService.govEntityDEtails.country !== null){
        this.submitGov2.controls["country"].setValue(this.commonService.govEntityDEtails.country);
      }
      if(this.commonService.govEntityDEtails.city !== undefined || this.commonService.govEntityDEtails.city !== null){
        this.submitGov2.controls["city"].setValue(this.commonService.govEntityDEtails.city);
      }
      // if(this.commonService.govEntityDEtails.rniFileName !== undefined || this.commonService.govEntityDEtails.rniFileName !== null){
      //   // this.rniFile.name = this.commonService.govEntityDEtails.rniFileName;
      //   this.rniFileName = this.commonService.govEntityDEtails.rniFileName;;
      // }
      // if(this.commonService.govEntityDEtails.lalFileName !== undefined || this.commonService.govEntityDEtails.lalFileName !== null){
      //   this.lalFile.name = this.commonService.govEntityDEtails.lalFileName;
      // }
      // if(this.commonService.govEntityDEtails.laglFileName !== undefined || this.commonService.govEntityDEtails.laglFileName !== null){
      //   this.laglFile.name = this.commonService.govEntityDEtails.laglFileName;
      // }

    }
    else{
      this.submitGov2.controls["address1"].setValue(null);
      this.submitGov2.controls["address2"].setValue(null);
      this.submitGov2.controls["country"].setValue(null);
      this.submitGov2.controls["city"].setValue(null);
      // this.rniFile.name = null;
      // this.lalFile.name = null;
      // this.laglFile.name = null;


    }
  }

  fileProgress(event: any, fileCat: String) {


    if (fileCat === 'rni') {
      let fileData = <File>event.target.files[0];


      if(fileData === undefined){
        this.rniFile = null;
        this.isSelectedrniFile = false;
      }

      if (fileData.size > 500000) {
        alert("Please upload < 500KB");
        fileData = undefined;
        this.rniFile = null;
        this.isSelectedrniFile = false;

      }
      let fname = fileData.name.split('.');
      if (!this.allowedExtensions.some((item) => item == (fname[fname.length - 1]).toLowerCase())) {
        alert( fname[fname.length - 1] + " file format is not allowed. Please upload recommended file types.");

      }
      else {
        this.rniFile = fileData;
        this.commonService.govEntityDEtails.rniFileName = fileData.name;
        this.isSelectedrniFile = true;

      }
    }
    else if (fileCat === 'lal') {

      let fileData = <File>event.target.files[0];

      if(fileData === undefined){
        this.lalFile = null;
        this.isSelectedlalFile = false;
      }

      if (fileData.size > 500000) {
        alert("Please upload < 500KB");
        fileData = undefined;
        this.lalFile = null;
        this.isSelectedlalFile = false;

      }
      let fname = fileData.name.split('.');
      if (!this.allowedExtensions.some((item) => item == (fname[fname.length - 1]).toLowerCase())) {
        alert( fname[fname.length - 1] + " file format is not allowed. Please upload recommended file types.");

      }
      else {
        this.lalFile = fileData;
        this.commonService.govEntityDEtails.lalFileName = fileData.name;
        this.isSelectedlalFile = true;
      }
    }
    else if (fileCat === 'lagl') {
      let fileData = <File>event.target.files[0];


      if(fileData === undefined){
        this.laglFile = null;
        this.isSelectedlaglFile = false;
      }

      if (fileData.size > 500000) {
        alert("Please upload < 500KB");
        fileData = undefined;
        this.laglFile = null;
        this.isSelectedlaglFile = false;

      }
      let fname = fileData.name.split('.');
      if (!this.allowedExtensions.some((item) => item == (fname[fname.length - 1]).toLowerCase())) {
        alert(fname[fname.length - 1] + " file format is not allowed. Please upload recommended file types.");

      }
      else {
        this.laglFile = fileData;
        this.commonService.govEntityDEtails.laglFileName = fileData.name;
        this.isSelectedlaglFile = true;
      }

    }

  }

  gotoPrevious() {
    this.router.navigate(['/governmentEntityReg']);
  }


  saveReg2() {
    console.log("Reg 2 data:::::" + JSON.stringify(this.submitGov2.value));
    if (this.submitGov2.valid) {
      this.commonService.govEntityDEtails.address1 = this.submitGov2.controls['address1'].value;
      this.commonService.govEntityDEtails.address2 = this.submitGov2.controls['address2'].value;
      this.commonService.govEntityDEtails.country = this.submitGov2.controls['country'].value;
      this.commonService.govEntityDEtails.city = this.submitGov2.controls['city'].value;
      this.commonService.govEntityDEtails.laglFile = this.laglFile;
      this.commonService.govEntityDEtails.rniFile = this.rniFile;
      this.commonService.govEntityDEtails.lalFile = this.lalFile;

      console.log(JSON.stringify(this.commonService.govEntityDEtails));
      this.commonService.isGovPart2Completed = true;
      this.router.navigate(['governmentEntityReg3']);

    }
    else{
      this.isOpenValidationAlert = true;
    }

  }

  resetForm() {
    if (this.submitGov2.valid) {
      console.log("Form Submitted!");
      this.submitGov2.reset();
    }
  }

  closeAlert(){
    this.isOpenValidationAlert = false;
  }

}
