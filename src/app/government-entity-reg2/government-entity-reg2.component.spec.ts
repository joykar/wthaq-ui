import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovernmentEntityReg2Component } from './government-entity-reg2.component';

describe('GovernmentEntityReg2Component', () => {
  let component: GovernmentEntityReg2Component;
  let fixture: ComponentFixture<GovernmentEntityReg2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovernmentEntityReg2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernmentEntityReg2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
